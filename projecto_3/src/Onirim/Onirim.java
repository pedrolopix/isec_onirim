package Onirim;

import Cartas.*;
import Estados.*;
import InterfaceTxt.ILog;
import java.util.ArrayList;
import java.util.List;

public class Onirim {

  private State state;
  private State stateFaseJogo = new StateFaseJogo(this);
  private State stateIniciarJogo = new StateIniciarJogo(this);
  private State stateComprar = new StateComprar(this);
  private State stateComprouPorta = new StateComprouPorta(this);
  private State stateComprouSonho = new StateComprouSonho(this);
  private State stateProfecia = new StateProfecia(this);
  private State stateGanhou = new StateGanhou(this);
  private State statePerdeu = new StatePerdeu(this);
  private State stateFinal = new StateFinal(this);
  private List<Baralho> baralhos = new ArrayList<>();
  private Cartas pilhaJogo = new Cartas(this, "jogo");
  private Cartas pilhaLimbo = new Cartas(this, "limbo");
  private Cartas pilhaDescarte = new Cartas(this, "descarte");
  private Cartas pilhaCartasJogadas = new Cartas(this, "jogadas");
  private Cartas mao = new Cartas(this, "mão");
  private ILog log;
  private boolean emJogo;

  //CONSTRUTOR
  public Onirim() {
    this.state = stateIniciarJogo;
  }
  
  //GET
  public boolean getEmJogo() { return emJogo; }
  public List<Baralho> getBaralhos() { return baralhos; }
  public Cartas getMao() { return mao; }
  public Cartas getPilhaJogo() { return pilhaJogo; }
  public Cartas getPilhaLimbo() { return pilhaLimbo; }
  public Cartas getPilhaDescarte() { return pilhaDescarte; }
  public Cartas getPilhaCartasJogadas() { return pilhaCartasJogadas; }
  public State getState() { return state; }
  public State getStateFaseJogo() { return stateFaseJogo; } 
  public State getStateComprar() { return stateComprar; } 
  public State getStateComprouSonho() { return stateComprouSonho; }
  public State getStateComprouPorta() { return stateComprouPorta; }
  public State getStateProfecia() { return stateProfecia; }
  public State getStateGanhou() { return stateGanhou; }
  public State getStateIniciarJogo() { return stateIniciarJogo; }
  public State getStatePerdeu() { return statePerdeu; }
  public State getStateFinal() { return stateFinal; }
  

  //SET
  private void setNextState(State state) {
    if (log != null) { log.estadoMudou(this.state, state); }
    this.state = state;
    //para estados que nao necessitam de interação com o user
    this.state.executaEstado(); 
  }
  public void setLog(ILog log) { this.log = log; } 
  public void nextStateIniciarJogo() { setNextState(stateIniciarJogo); }
  public void nextStateFaseJogo() { setNextState(stateFaseJogo); }
  public void nextStateComprar() { setNextState(stateComprar); }
  public void nextStateComprouSonho() { setNextState(stateComprouSonho); }
  public void nextStateComprouPorta() { setNextState(stateComprouPorta); } 
  public void nextStateProfecia() { setNextState(stateProfecia); }
  public void nextStatePerdeu() { setNextState(statePerdeu); }
  public void nextStateGanhou() { setNextState(stateGanhou); }
  public void nextStateFinal() { setNextState(stateFinal); }
  
    
  //############################################################################
  //##
  //## FUNÇÕES DE COMUNICAÇÃO COM O INTERFACE TEXTO
  public void executarStateIniciarJogo(int opcao){
    this.state.iniciarJogo(opcao);
  }
  
  public void executarStateFaseJogo(int opcao, Carta carta){
    this.state.faseJogo(opcao, carta);
  }
  
  public void executarStateComprouPorta(int opcao) {
    this.state.comprouPorta(opcao);
  }
  
  public void executarStateComprouSonho(int opcao) {
    this.state.comprouSonho(opcao);
  }
  
  public void executarStateProfecia() {
    this.state.profecia();
  }
 
    
  
  //SE HOUVER LIMBO JUNTA E BARALHA JOGO
  public void baralhar() {
    if (!pilhaLimbo.isEmpty()) {
      doBaralharLimbo();
      pilhaJogo.addAll(pilhaLimbo);
      pilhaLimbo.clear();
      pilhaJogo.baralhar();
      doBaralhar();
    }
  }

  //SIMBOLO != DO ULTIMO JOGADO = JOGADA VALIDA
  public boolean jogadaValida(Carta carta) {
    //Para ajudar na batota
    //se crata != Labirinto é sempre jogada valida
    if( !(carta instanceof CartaLabirinto) ){
      return true;
    }
    
    if (pilhaCartasJogadas.isEmpty()) {
      return true;
    }
    else {
      Carta ultimaJogada = pilhaCartasJogadas.get(pilhaCartasJogadas.size() - 1);
      if (carta.getSimbolo().equals(ultimaJogada.getSimbolo())) {
        return false;
      }
      return true;
    }
  }

  //3 CARTAS LABIRINTO CONSECUTIVAS DA MESMA COR
  public boolean coresConsecutivas() {
    int numCores = 3;
    if (pilhaCartasJogadas.size() < numCores) {
      return false;
    }

    int ultima = pilhaCartasJogadas.size() - 1;
    int coresIguais = 1;
    Cores ultimaCor = pilhaCartasJogadas.get(ultima).getCor();

    for (int i = ultima - 1; i > ultima - numCores; i--) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        return false;
      }
      if (pilhaCartasJogadas.get(i).getCor().compareTo(ultimaCor) == 0) {
        coresIguais++;
      }
    }
    if (coresIguais == 3) {
      return true;
    }
    return false;
  }

  //ESTAO 8 PORTAS NA PILHA DE CARTAS JOGADAS ?
  public boolean encontrouTodasPortas() {
    int totalPortas = 8;
    int portasJogadas = 0;

    for (int i = 0; i < pilhaCartasJogadas.size(); i++) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        portasJogadas++;
      }
    }
    if (portasJogadas == totalPortas) {
      return true;
    }
    return false;
  }

  //INICIA MAO DE 5 CARTAS SEM EFEITO DE COMPRA
  public void iniciaMao() {
    doIniciaMao();
    do {
      Carta c = pilhaJogo.tiraUltimaCarta();
      if (c instanceof CartaLabirinto) {
        mao.add(c);
      }
      else {
        pilhaLimbo.add(c);
      }
    } while (mao.size() != 5);
  } 
  
  //############################################################################
  //## LOG DO JOGO
  //##
  public void doJogadaInvalida(Carta carta) {
    if (log != null) {
      log.jogadaInvalida(carta);
    }
  }

  public void doEncontrouPorta(Carta carta) {
    if (log != null) {
      log.encontrouPorta(carta);
    }
  }

  public void doNaoExisteChaveNaMao() {
    if (log != null) {
      log.naoExisteChaveNaMao();
    }
  }

  public void doNaoExistePortaNoJogo() {
    if (log != null) {
      log.NaoExistePortaNoJogo();
    }
  }

  public void doBaralharLimbo() {
    if (log != null) {
      log.baralharLimbo();
    }
  }

  public void doBaralhar() {
    if (log != null) {
      log.baralhar();
    }
  }

  public void doIniciaMao() {
    if (log != null) {
      log.iniciaMao();
    }
  }

  public void doMoveCarta(Cartas origem, Carta carta, Cartas destino) {
    if (log != null) {
      log.moveCarta(origem, carta, destino);
    }
  }

  public void doGanhouJogo() {
    if (log != null) {
      log.ganhouJogo();
    }
  }

  public void doPerdeuJogo() {
    if (log != null) {
      log.perdeuJogo();
    }
  }

  public void doCartaSonhoDescartada() {
    if (log != null) {
      log.cartaSonhoDescartada();
    }
  }

  public void doCartaPortaDescartada(CartaPorta carta) {
    if (log != null) {
      log.cartaPortaDescartada(carta);
    }
  }

  public void doCartaLabirintoDescartada(CartaLabirinto carta) {
    if (log != null) {
      log.cartaLabirintoDescartada(carta);
    }
  }

  public void doNaoExistemCartasSuficientes(int nPesadelo) {
    if (log != null) {
      log.naoExistemCartasSuficientes(nPesadelo);
    }
  }

  public void doTerminou() {
    if (log != null){
      log.terminouJogo();
    }
  }

  

  

  

  

  
}