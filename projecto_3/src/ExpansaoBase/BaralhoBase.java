package ExpansaoBase;

import Onirim.Baralho;
import Cartas.CartaLabirinto;
import Cartas.CartaPorta;
import Cartas.CartaSonho;
import Cartas.Cores;
import Cartas.Simbolos;

public class BaralhoBase extends Baralho {

  public BaralhoBase() {
    super();
  }

  private void AdicionaCartaLabirinto(int qtd, Cores cor, Simbolos simbolo) {
    for (int i = 0; i < qtd; i++) {
      getCartas().add(new CartaLabirinto(cor, simbolo));
    }
  }

  private void AdicionaPesadelos(int qtd) {
    for (int i = 0; i < qtd; i++) {
      getCartas().add(new CartaSonho());
    }
  }

  @Override
  public void CriarCartas() {
    //Criar Cartas Porta
    for (int i = 0; i < 2; i++) {
      for (Cores c : Cores.values()) {
        getCartas().add(new CartaPorta(c));
      }
    }
    //Criar Cartas Labirinto
    AdicionaCartaLabirinto(9, Cores.encarnado, Simbolos.sol);
    AdicionaCartaLabirinto(4, Cores.encarnado, Simbolos.lua);
    AdicionaCartaLabirinto(3, Cores.encarnado, Simbolos.chave);
    AdicionaCartaLabirinto(8, Cores.azul, Simbolos.sol);
    AdicionaCartaLabirinto(4, Cores.azul, Simbolos.lua);
    AdicionaCartaLabirinto(3, Cores.azul, Simbolos.chave);
    AdicionaCartaLabirinto(7, Cores.verde, Simbolos.sol);
    AdicionaCartaLabirinto(4, Cores.verde, Simbolos.lua);
    AdicionaCartaLabirinto(3, Cores.verde, Simbolos.chave);
    AdicionaCartaLabirinto(6, Cores.castanho, Simbolos.sol);
    AdicionaCartaLabirinto(4, Cores.castanho, Simbolos.lua);
    AdicionaCartaLabirinto(3, Cores.castanho, Simbolos.chave);

    //Criar Cartas Pesadelo
    AdicionaPesadelos(10);
  }
}
