package InterfaceTxt;

import Cartas.Carta;
import Cartas.Cartas;
import Estados.*;
import Onirim.Onirim;
import java.util.Scanner;

public class Consola {

  private Onirim onirim;

  //CONSTRUTOR
  public Consola(Onirim onirim) {
    this.onirim = onirim;
  }

  //INICIO DA ACCAO
  public void run() {
    while (!(onirim.getState() instanceof StateFinal)) {
      interagirComUtilizador();
    }
  }

  //APRESENTAÇÃO DE MENUS PARA INTERACCAO COM O UTILIZADOR
  public void interagirComUtilizador() {
    State state = onirim.getState();

    if (state instanceof StateIniciarJogo) {
      int opcao = menuInicial();
      onirim.executarStateIniciarJogo(opcao);
    }

    if (state instanceof StateComprouPorta) {
      int opcao;
      Carta cartaPorta = onirim.getPilhaJogo().getUltima();
      Carta cartaChave = onirim.getMao().temChave(cartaPorta.getCor());
      if (cartaChave == null) {
        opcao = 2;
      }
      else {
        opcao = menuComprouPorta(cartaChave);
      }
      onirim.executarStateComprouPorta(opcao);
    }

    if (state instanceof StateComprouSonho) {
      int opcao = menuComprouSonho();
      onirim.executarStateComprouSonho(opcao);
    }

    if (state instanceof StateProfecia) {
      Carta carta;
      Cartas pilhaAuxiliar = new Cartas(onirim, "");

      System.out.println("===================================================");
      System.out.println(" PROFECIA");
      System.out.println("===================================================");

      for (int i = 0; i < 5; i++) {
        carta = onirim.getPilhaJogo().tiraUltimaCarta();
        pilhaAuxiliar.add(carta);
      }
      System.out.println("Cartas da Profecia" + pilhaAuxiliar);
      carta = escolheCarta("Qual pretende descartar", pilhaAuxiliar, -1);
      pilhaAuxiliar.moverCartaPara(carta, onirim.getPilhaDescarte());
      System.out.println("__________________________________________");
      System.out.println("Organizar as cartas de volta para o jogo");
      while (!pilhaAuxiliar.isEmpty()) {
        System.out.println("__________________________________________");
        System.out.println("Cartas da Profecia" + pilhaAuxiliar);
        carta = escolheCarta("Carta", pilhaAuxiliar, -1);
        pilhaAuxiliar.moverCartaPara(carta, onirim.getPilhaJogo());
      }
      onirim.executarStateProfecia();
    }


    if (state instanceof StateFaseJogo) {
      Carta carta = null;
      int opcao = menuFaseJogo();
      onirim.executarStateFaseJogo(opcao, carta);
    }


    if (state instanceof StateFaseJogo) {
      Carta carta = null;
      int opcao = menuFaseJogo();
      //esta com o switch por causa da batota
      //deveria simplesmente fazer: onirim.executarStateFaseJogo(opcao, carta);
      //Jogar / Descartar / Terminar
      switch (opcao) {
        case 1:
          carta = escolheCarta("Carta para jogar", onirim.getMao(), -1);
          if (carta != null) {
            onirim.executarStateFaseJogo(opcao, carta);
          }
          break;
        case 2:
          carta = escolheCarta("Carta para descartar", onirim.getMao(), -1);
          if (carta != null) {
            onirim.executarStateFaseJogo(opcao, carta);
          }
          break;
        case 3:
          mostrar7CartasJogo();
          break;
        case 4:
          carta = escolheCarta("Carta para colocar na mao", onirim.getPilhaJogo(), -1);
          //cartaMao = escolheCarta("Qual pretende substituir?", onirim.getMao(), -1);
          //onirim.getMao().moverCartaPara(cartaMao, onirim.getPilhaLimbo());
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getMao());
          break;
        case 5:
          carta = escolheCarta("Carta para o topo do baralho", onirim.getPilhaJogo(), -1);
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getPilhaJogo());
          break;
        case 6:
          carta = escolheCarta("Carta para a mesa de jogo", onirim.getPilhaJogo(), -1);
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getPilhaCartasJogadas());
          break;
        case 7:
          mostraCartasJogo();
          mostraCartasDescarte();
          mostraCartasLimbo();
          break;
        case 8:
          onirim.executarStateFaseJogo(opcao, carta);
      }
    }
  }

  //############################################################################
  //## Menus - (implementação dos interfaces)
  //##
  public int menuInicial() {
    int opcao;
    do {
      System.out.println("===================================================");
      System.out.println("====================   ONIRIM   ===================");
      System.out.println("===================================================");
      System.out.println("1. Jogar");
      System.out.println("2. Terminar");
      System.out.println("");
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > 2);
    return opcao;
  }

  public int menuFaseJogo() {
    int opcao = 0;
    System.out.println("\n");
    System.out.println("===================================================");
    mostraCartasJogadas();
    mostraMao();
    System.out.println("===================================================");
    String[] opcoes = {"Jogar", "Descartar", "Ver 7 cartas",
                       "Acrescentar na Mão", "Acrescentar ao Baralho",
                       "Acrescentar a Mesa", "Mostrar restantes cartas",
                       "Terminar"};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > 8);
    return opcao;
  }

  public int menuComprouPorta(Carta cartaChave) {
    int opcao = 0;
    System.out.println("===================================================");
    System.out.println(" COMPROU PORTA");
    System.out.println("===================================================");
    System.out.println("Acabou de comprar uma porta " + cartaChave.getCor());
    String[] opcoes = {"Descartar chave e colocar porta em jogo ?",
                       "Colocar porta comprada no Limbo ?"};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > 2);
    return opcao;
  }

  public int menuComprouSonho() {
    int opcao = 0;
    System.out.println("===================================================");
    System.out.println(" COMPROU PESADELO");
    System.out.println("===================================================");

    String[] opcoes = {"Descartar da mão uma Carta Chave.",
                       "Enviar uma Carta Porta ja jogada para o Limbo.",
                       "Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.",
                       "Descartar Mão e comprar nova, tal como inicialmente."};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > 5);
    return opcao;
  }

  //############################################################################
  //## FUNÇÕES AUXILIARES
  //##
  public void mostraMao() {
    System.out.println(onirim.getMao());
  }

  public void mostraCartasJogo() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaJogo());
  }

  public void mostraCartasJogadas() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaCartasJogadas());
  }

  public void mostraCartasLimbo() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaLimbo());
  }

  public void mostraCartasDescarte() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaDescarte());
  }

  public void mostrar7CartasJogo() {
    System.out.print("Ultimas 7 cartas de jogo");
    System.out.println(onirim.getPilhaJogo().getCartasJogo(7));
  }

  //PEDE NOME DA CARTA DE UMA DADA PILHA DE CARTAS
  Carta escolheCarta(String accao, Cartas cartas, int nCartasFim) {
    String str;
    Carta carta = null;
    do {
      System.out.print(accao + ": ");
      str = lerString();
      carta = cartas.getCarta(str, nCartasFim);
      if (carta == null) {
        System.out.println("A carta " + str + " não existe, escolha outra.");
      }
    } while (carta == null);
    return carta;
  }

  private String lerString() {
    Scanner sc = new Scanner(System.in);
    return sc.nextLine();
  }

  private int lerInt() {
    Scanner s = new Scanner(System.in);
    while (!s.hasNextInt()) {
      s.next();
    }
    return s.nextInt();
  }
}
