package InterfaceTxt;

import Cartas.Carta;
import Cartas.Cartas;
import Estados.State;

public interface ILog {

  public void estadoMudou(State oldState, State newState);

  public void jogadaInvalida(Carta carta);

  public void encontrouPorta(Carta carta);

  public void naoExisteChaveNaMao();

  public void NaoExistePortaNoJogo();

  public void baralharLimbo();

  public void baralhar();

  public void iniciaMao();

  public void moveCarta(Cartas origem, Carta carta, Cartas destino);

  public void perdeuJogo();

  public void ganhouJogo();

  public void cartaSonhoDescartada();

  public void cartaPortaDescartada(Carta carta);

  public void cartaLabirintoDescartada(Carta carta);

  public void naoExistemCartasSuficientes(int nPesadelo);

  public void terminouJogo();
}
