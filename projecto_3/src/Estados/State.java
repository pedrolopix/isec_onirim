package Estados;

import Cartas.Carta;
import Onirim.Onirim;

public abstract class State {

  private Onirim onirim;
  

  //CONSTRUTOR
  public State(Onirim onirim) {
    this.onirim = onirim;
  }

  //GET
  public Onirim getOnirim() {
    return onirim;
  }

  

  public void executaEstado() {}
  
  public void iniciarJogo(int opcao){}
  
  public void faseJogo(int opcao, Carta carta){}
  
  public void comprouPorta(int opcao) {}

  public void comprouSonho(int opcao) {}

  public void profecia() {}  
}
