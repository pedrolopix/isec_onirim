package Estados;

import Onirim.Onirim;

public class StatePerdeu extends State {

  public StatePerdeu(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    getOnirim().doPerdeuJogo();
    getOnirim().nextStateFinal();    
  }

  @Override
  public String toString() {
    return "Perdeu";
  }
}
