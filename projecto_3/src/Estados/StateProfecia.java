package Estados;

import Onirim.Onirim;

public class StateProfecia extends State {

  public StateProfecia(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void profecia() {
    super.profecia();
    getOnirim().nextStateComprar();
  }
 
  @Override
  public String toString() {
    return "Profecia";
  }
}
