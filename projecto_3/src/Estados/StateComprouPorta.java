package Estados;

import Cartas.Carta;
import Onirim.Onirim;

public class StateComprouPorta extends State {

  //CONSTRUTOR
  public StateComprouPorta(Onirim onirim) {
    super(onirim);
  }
  
  @Override
  public void comprouPorta(int opcao) {
    Carta cartaPorta = getOnirim().getPilhaJogo().getUltima();
    Carta cartaChave = getOnirim().getMao().temChave(cartaPorta.getCor());
    switch(opcao){
      case 1 : descartarChaveColocarPortaJogo(cartaChave, cartaPorta); break;
      case 2 : colocarPortaLimbo(cartaPorta); break;
    }
  }
  
  private void descartarChaveColocarPortaJogo(Carta cartaChave, Carta cartaPorta) {
    getOnirim().getMao().moverCartaPara(cartaChave, getOnirim().getPilhaDescarte());
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaCartasJogadas());
    if (getOnirim().encontrouTodasPortas()) {
      getOnirim().nextStateGanhou();
    }
    else{
      getOnirim().nextStateComprar();
    }
  }

  private void colocarPortaLimbo(Carta cartaPorta) {
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }  

  @Override
  public String toString() {
    return "Comprou Porta";
  }

  
}
