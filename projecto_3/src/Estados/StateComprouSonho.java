package Estados;

import Cartas.Carta;
import Cartas.CartaPorta;
import Cartas.CartaSonho;
import Onirim.Onirim;

public class StateComprouSonho extends State {

  //CONSTRUTOR
  public StateComprouSonho(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void comprouSonho(int opcao) {
    super.comprouSonho(opcao);
    switch(opcao){
      case 1 : executaPesadeloUm(); break;
      case 2 : executaPesadeloDois(); break;
      case 3 : executaPesadeloTres(); break;
      case 4 : executaPesadeloQuatro(); break;
    }
  } 
                
  //1 - Descartar da mao uma Carta Chave.
  private void executaPesadeloUm() {
    Carta carta = getOnirim().getMao().temChave();
    if (carta != null) {
      getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().doNaoExisteChaveNaMao();
    getOnirim().nextStateComprouSonho();
  }

  //2 - Enviar uma Carta Porta ja jogada para o Limbo.
  private void executaPesadeloDois() {
    //TODO porta a escolha do utilizador, ou ultima jogada ???
    Carta carta = getOnirim().getPilhaCartasJogadas().getNextPorta();
    if (carta != null) {
      getOnirim().getPilhaCartasJogadas().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().doNaoExistePortaNoJogo();
    getOnirim().nextStateComprouSonho();
  }

  //3 - Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.
  private void executaPesadeloTres() {
    int ver = 5;

    if (getOnirim().getPilhaJogo().isEmpty()) {
      getOnirim().nextStatePerdeu();
      return;
    }

    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(3);
      getOnirim().nextStateComprouSonho();
      return;
    }

    int ultima = getOnirim().getPilhaJogo().size() - 1;
    for (int i = ultima; i > ultima - ver && i > 0; i--) {
      Carta carta = getOnirim().getPilhaJogo().get(i);
      if ((carta instanceof CartaSonho) || (carta instanceof CartaPorta)) {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      }
      else {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      }
    }
    getOnirim().nextStateComprar();
  }

  //4 - Descartar Mao e comprar nova, tal como inicialmente.
  private void executaPesadeloQuatro() {
    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(4);
      getOnirim().nextStateComprouSonho();
      //TODO mesmo com +de 5 cratas, verificar se existem 5 labirintos ???
      return;
    }
    getOnirim().getPilhaDescarte().addAll(getOnirim().getMao());
    getOnirim().getMao().clear();
    getOnirim().iniciaMao();
    getOnirim().nextStateComprar();
  }

  @Override
  public String toString() {
    return "Comprou Sonho";
  }
}
