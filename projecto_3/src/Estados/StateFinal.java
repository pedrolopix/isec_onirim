package Estados;

import Onirim.Onirim;

public class StateFinal extends State {

  //CONSTRUTOR
  public StateFinal(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    getOnirim().doTerminou();
  } 

  @Override
  public String toString() {
    return "Terminou Jogo";
  }
  
}
