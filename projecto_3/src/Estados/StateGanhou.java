package Estados;

import Onirim.Onirim;

public class StateGanhou extends State {
  
  //CONSTRUTOR
  public StateGanhou(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    getOnirim().doGanhouJogo();
    getOnirim().nextStateFinal();    
  }  

  @Override
  public String toString() {
    return "Ganhou";
  }
}
