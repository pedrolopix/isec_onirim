package Estados;

import ExpansaoBase.BaralhoBase;
import Onirim.Onirim;

public class StateIniciarJogo extends State {

  //CONSTRUTOR
  public StateIniciarJogo(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void iniciarJogo(int opcao) {
    switch(opcao){
      case 1 : novoJogo(); break; //JOGAR 
      case 2 : terminarOnirim(); break; //TERMINAR
    }
  }
  
  private void novoJogo(){
    getOnirim().getPilhaJogo().clear();
    getOnirim().getPilhaLimbo().clear();
    getOnirim().getPilhaDescarte().clear();
    getOnirim().getMao().clear();
    getOnirim().getPilhaCartasJogadas().clear();

    //CRIA BARALHO BASE E COLOCA NOS BARALHOS DE JOGO
    getOnirim().getBaralhos().add(new BaralhoBase());
    for (int i = 0; i < getOnirim().getBaralhos().size(); i++) {
      getOnirim().getPilhaJogo().addAll(getOnirim().getBaralhos().get(i).getCartas());
    }

    getOnirim().getPilhaJogo().baralhar();
    getOnirim().iniciaMao();
    getOnirim().baralhar();
    
    getOnirim().nextStateFaseJogo();
  }
  
  private void terminarOnirim() {
    getOnirim().nextStateFinal();
  }
  

  @Override
  public String toString() {
    return "Novo Jogo";
  }

  
}
