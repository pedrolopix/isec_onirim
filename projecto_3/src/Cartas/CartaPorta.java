package Cartas;

import Onirim.Onirim;

public class CartaPorta extends CartaCor {

  public CartaPorta(Cores cor) {
    super(cor);
  }

  @Override
  public void foiComprada(Onirim onirim) {
    onirim.nextStateComprouPorta();
  }

  @Override
  public Simbolos getSimbolo() {
    return null;
  }

  @Override
  public Cores getCor() {
    return super.getCor();
  }

  @Override
  public String toString() {
    return super.toString() + "P";
  }

  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.doCartaPortaDescartada(this);
  }
}
