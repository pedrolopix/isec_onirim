package Cartas;

import Onirim.Onirim;

public class CartaLabirinto extends CartaCor {

  private Simbolos simbolo;

  public CartaLabirinto(Cores cor, Simbolos simbolo) {
    super(cor);
    this.simbolo = simbolo;
  }

  @Override
  public void foiComprada(Onirim onirim) {
    onirim.getPilhaJogo().moverCartaPara(this, onirim.getMao());
    onirim.nextStateComprar();
  }

  @Override
  public Simbolos getSimbolo() {
    return simbolo;
  }

  @Override
  public Cores getCor() {
    return super.getCor();
  }

  @Override
  public String toString() {
    return super.toString() + simbolo.toString().toUpperCase().substring(0, 1);
  }

  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.getMao().moverCartaPara(this, onirim.getPilhaDescarte());
    onirim.doCartaLabirintoDescartada(this);
    if (this.getSimbolo().compareTo(Simbolos.chave) == 0) {
      onirim.nextStateProfecia();
      return;
    }
    onirim.nextStateComprar();
  }
}
