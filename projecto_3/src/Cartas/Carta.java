package Cartas;

import Onirim.Onirim;

public abstract class Carta {

  public abstract void foiComprada(Onirim onirim);

  public abstract void foiDescartada(Onirim onirim);

  public abstract Simbolos getSimbolo();

  public abstract Cores getCor();

  @Override
  public abstract String toString();
}
