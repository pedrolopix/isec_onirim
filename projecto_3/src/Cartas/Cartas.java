package Cartas;

import ExpansaoBase.BaralhoBase;
import Onirim.Onirim;
import java.util.ArrayList;
import java.util.Collections;

public class Cartas {

  private ArrayList<Carta> lista = new ArrayList<>();
  private Onirim onirim;
  private String nome;

  //CONSTRUTOR
  public Cartas(Onirim onirim, String nome) {
    this.nome = nome;
    this.onirim = onirim;
  }

  //GET
  public ArrayList<Carta> getLista() {
    return lista;
  }
  
  public Carta get(int i) {
    return lista.get(i);
  }
  
  public void clear() {
    lista.clear();
  }

  public boolean isEmpty() {
    return lista.isEmpty();
  }

  public int size() {
    return lista.size();
  } 

  public void add(Carta carta) {
    lista.add(carta);
  }

  public void addAll(ArrayList<Carta> cartas) {
    lista.addAll(cartas);
  }

  public void addAll(Cartas cartas) {
    lista.addAll(cartas.lista);
  }

  public void add(BaralhoBase baralhoBase) {
    lista.addAll(baralhoBase.getCartas());
  }

  public Carta tiraUltimaCarta() {
    if (lista.isEmpty()) {
      return null;
    }
    Carta c = getUltima();
    lista.remove(c);
    return c;
  } 

  public Carta getNextPorta() {
    for (int i = lista.size() - 1; i >= 0; i--) {
      if (lista.get(i) instanceof CartaPorta) {
        return lista.get(i);
      }
    }
    return null;
  }
  
  public Carta getNextPorta(Cores cor) {
    for (int i = lista.size() - 1; i >= 0; i--) {
      if (lista.get(i) instanceof CartaPorta && (lista.get(i).getCor() == cor)) {
        return lista.get(i);
      }
    }
    return null;
  }
  
  public Carta getCarta(String nome) {
    return getCarta(nome, lista.size());
  }

  public Carta getCarta(String nome, int nCartasFim) {
    int ultima = lista.size() - 1;
    if (nCartasFim == -1) {
      nCartasFim = ultima;
    }
    for (int i = ultima; i >= ultima - nCartasFim; i--) {
      if (lista.get(i).toString().equalsIgnoreCase(nome)) {
        return lista.get(i);
      }
    }
    return null;
  }
  
  public Carta temChave() {
    for (int i = 0; i < lista.size(); i++) {
      if (lista.get(i).getSimbolo().compareTo(Simbolos.chave) == 0) {
        return lista.get(i);
      }
    }
    return null;
  }

  public Carta temChave(Cores cor) {
    for (int i = 0; i < lista.size(); i++) {
      if (lista.get(i).getSimbolo().compareTo(Simbolos.chave) == 0
              && lista.get(i).getCor().compareTo(cor) == 0) {
        return lista.get(i);
      }
    }
    return null;
  }

  public ArrayList<Carta> getSonhos() {
    ArrayList<Carta> s = new ArrayList<>();
    for (int i = 0; i < lista.size(); i++) {
      if (lista.get(i) instanceof CartaSonho) {
        s.add(lista.get(i));
      }
    }
    return s;
  }

  public void moverCartaPara(Carta carta, Cartas cartas) {
    onirim.doMoveCarta(this, carta, cartas);
    lista.remove(carta);
    cartas.add(carta);
  }

  public void baralhar() {
    Collections.shuffle(lista);
  }

  public Cartas getCartasJogo(int ncartas) {
    Cartas aux = new Cartas(onirim, "");
    int ultima = lista.size() - 1;
    for (int i = ultima; i > ultima - ncartas && i > 0; i--) {
      aux.add(lista.get(i));
    }
    return aux;
  }
  
  public String getNome() {
    return nome;
  }

  public Carta getUltima() {
    if (lista.isEmpty()) {
      return null;
    }
    Carta c = lista.get(lista.size() - 1);
    return c;
  }
  
  @Override
  public String toString() {
    String str = nome + ": ";

    if (isEmpty()) {
      return str + " Sem cartas";
    }

    for (int i = 0; i < lista.size(); i++) {
      str += (lista.get(i).toString());
      if (i < lista.size() - 1) {
        str += ", ";
      }
    }
    return str;
  }
}
