/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Onirim;

import java.util.List;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class OnirimTest {
  
  public OnirimTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }
  
  @Before
  public void setUp() {
  }
  
  @After
  public void tearDown() {
  }


  /**
   * Test of coresConsecutivas method, of class Onirim.
   */
  @Test
  public void testCoresConsecutivas3() {
    System.out.println("coresConsecutivas3");
    Onirim onirim = new Onirim();
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.lua));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.sol));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.lua));  
    assertEquals(true, onirim.coresConsecutivas());

  }

  
    /**
   * Test of coresConsecutivas method, of class Onirim.
   */
  @Test
  public void testCoresConsecutivas() {
    System.out.println("coresConsecutivas");
    Onirim onirim = new Onirim();
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.azul,Simbolos.lua));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.azul,Simbolos.sol));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.lua));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.sol));  
    assertEquals(false, onirim.coresConsecutivas());
    onirim.getPilhaCartasJogadas().add(new CartaLabirinto(Cores.verde,Simbolos.lua));  
    assertEquals(true, onirim.coresConsecutivas());

  }


}
