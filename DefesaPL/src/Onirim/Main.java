package Onirim;

public class Main {

  public static void main(String[] args) {

    Onirim onirim = new Onirim();

    onirim.setLog(new ConsolaLog());

    Consola c = new Consola(onirim);

    c.run();
  }
}
