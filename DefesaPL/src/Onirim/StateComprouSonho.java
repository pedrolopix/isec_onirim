package Onirim;

class StateComprouSonho extends State {

  //CONSTRUTOR
  public StateComprouSonho(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    //executaPesadeloQuatro();
  }

  //1 - Descartar da mao uma Carta Chave.
  public void executaPesadeloUm() {
    Carta carta = getOnirim().getMao().temChave();
    if (carta != null) {
      getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().doNaoExisteChaveNaMao();
  }

  //2 - Enviar uma Carta Porta ja jogada para o Limbo.
  public void executaPesadeloDois() {
    //TODO porta a escolha do utilizador, ou ultima jogada ???
    Carta carta = getOnirim().getPilhaCartasJogadas().getNextPorta();
    if (carta != null) {
      getOnirim().getPilhaCartasJogadas().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().doNaoExistePortaNoJogo();
  }

  //3 - Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.
  public void executaPesadeloTres() {
    int ver = 5;

    if (getOnirim().getPilhaJogo().isEmpty()) {
      getOnirim().nextStatePerdeu();
      return;
    }

    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(3);
      return;
    }

    int ultima = getOnirim().getPilhaJogo().size() - 1;
    for (int i = ultima; i > ultima - ver && i > 0; i--) {
      Carta carta = getOnirim().getPilhaJogo().get(i);
      if ((carta instanceof CartaSonho) || (carta instanceof CartaPorta)) {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      }
      else {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      }
    }
    getOnirim().nextStateComprar();
  }

  //4 - Descartar Mao e comprar nova, tal como inicialmente.
  public void executaPesadeloQuatro() {
    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(4);
      //TODO mesmo com +de 5 cratas, verificar se existem 5 labirintos ???
      return;
    }

    getOnirim().getPilhaDescarte().addAll(getOnirim().getMao());

    getOnirim().getMao().clear();

    getOnirim().iniciaMao();

    getOnirim().nextStateComprar();
  }

  @Override
  public String toString() {
    return "Comprou Sonho";
  }
}
