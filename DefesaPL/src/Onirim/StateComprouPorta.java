package Onirim;

class StateComprouPorta extends State {

  //CONSTRUTOR
  public StateComprouPorta(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    Carta cartaPorta = getOnirim().getPilhaJogo().getUltima();
    Carta cartaChave = getOnirim().getMao().temChave(cartaPorta.getCor());

    //se tiver chave da mm cor da porta comprada
    if (cartaChave != null) {
      super.executaEstado();
      return;
    }

    //envia porta para o limbo se nao tiver chave
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }

  @Override
  public String toString() {
    return "Comprou Porta";
  }
}
