package Onirim;

public class StateJogar extends State {

  public StateJogar(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
  }

  @Override
  public void descarta(Carta carta) {
    carta.foiDescartada(getOnirim());
  }

  @Override
  public void joga(Carta carta) {
    if (carta == null) {
      return;
    }
    
    if (!getOnirim().jogadaValida(carta)) {
      getOnirim().doJogadaInvalida(carta);
      return;
    }

    //efectiva a jogada movendo da mao para as cartas jogadas
    getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaCartasJogadas());

    //se 8 portas jogadas, ganhou! (BATOTA - carta n se joga da mao) 
    if (getOnirim().encontrouTodasPortas()) {
      //getOnirim().ganhou();
      getOnirim().nextStateGanhou();
      return;
    }
    
    //verificar sequencia de 3 cartas labirinto da mesma cor
    if (getOnirim().coresConsecutivas()) {
      encontrouPorta(carta);
      return;
    }
    getOnirim().nextStateComprar();
  }
  
  //3 CARTAS LABIRINTO CONSECUTIVAS DA MESMA COR
  private void encontrouPorta(Carta carta) {
    getOnirim().doEncontrouPorta(carta);
    Carta porta = getOnirim().getPilhaJogo().getNextPorta(carta.getCor());
    //se não há porta da mesma cor
    if (porta == null) {
      getOnirim().nextStateComprar();
      return;
    }

    getOnirim().getPilhaJogo().moverCartaPara(porta, getOnirim().getPilhaCartasJogadas());
    
    //se 8 portas em jogadas, ganhou!
    if (getOnirim().encontrouTodasPortas()) {
      //getOnirim().ganhou();
      getOnirim().nextStateGanhou();
      return;
    }

    getOnirim().nextStateComprar();
  }
  
  @Override
  public String toString() {
    return "Joga";
  }
}
