package Onirim;

public class StateGanhou extends State {

  public StateGanhou(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    getOnirim().ganhou();
    getOnirim().doGanhouJogo();
    
  }

  @Override
  public String toString() {
    return "Ganhou";
  }
}
