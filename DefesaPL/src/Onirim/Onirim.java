package Onirim;

import java.util.ArrayList;
import java.util.List;

public class Onirim {

  private State state;
  private State stateJogar = new StateJogar(this);
  private State stateIniciarJogo = new StateIniciarJogo(this);
  private State stateComprar = new StateComprar(this);
  private State stateComprouPorta = new StateComprouPorta(this);
  private State stateComprouSonho = new StateComprouSonho(this);
  private State stateProfecia = new StateProfecia(this);
  private State stateGanhou = new StateGanhou(this);
  private State statePerdeu = new StatePerdeu(this);
  private State stateDefesa1 = new StateDefesa1(this);
  private List<Baralho> baralhos = new ArrayList<>();
  private Cartas pilhaJogo = new Cartas(this, "jogo");
  private Cartas pilhaLimbo = new Cartas(this, "limbo");
  private Cartas pilhaDescarte = new Cartas(this, "descarte");
  private Cartas pilhaCartasJogadas = new Cartas(this, "jogadas");
  private Cartas mao = new Cartas(this, "mão");
  private ILog log;
  private boolean emJogo;

  //COLOCA ESTADO DE ARRANQUE DO ONIRIM
  public void iniciarJogo() {
    state = stateIniciarJogo;
    emJogo = true;
  }
  
  //WHILE emJogo = TRUE
  void executaEstado() {
    state.executaEstado();
  }

  //GET
  boolean getEmJogo() { return emJogo; }
  public List<Baralho> getBaralhos() { return baralhos; }
  public Cartas getMao() { return mao; }
  public Cartas getPilhaJogo() { return pilhaJogo; }
  public Cartas getPilhaLimbo() { return pilhaLimbo; }
  public Cartas getPilhaDescarte() { return pilhaDescarte; }
  public Cartas getPilhaCartasJogadas() { return pilhaCartasJogadas; }
  public State getStateJogar() { return stateJogar; } 
  public State getStateComprar() { return stateComprar; } 
  public State getStateComprouSonho() { return stateComprouSonho; }
  public State getStateComprouPorta() { return stateComprouPorta; }
  public State getStateProfecia() { return stateProfecia; }
  public State getStateGanhou() { return stateGanhou; }
  public State getStateIniciarJogo() { return stateIniciarJogo; }
  public State getStatePerdeu() { return statePerdeu; }
  public State getStateDefesa1() { return stateDefesa1; }

  //SET
  public void setLog(ILog log) { this.log = log; } 
  void nextStateJogar() { setNextState(stateJogar); }
  void nextStateComprar() { setNextState(stateComprar); }
  void nextStateComprouSonho() { setNextState(stateComprouSonho); }
  void nextStateComprouPorta() { setNextState(stateComprouPorta); } 
  void nextStateProfecia() { setNextState(stateProfecia); }
  void nextStatePerdeu() { setNextState(statePerdeu); }
  void nextStateGanhou() { setNextState(stateGanhou); }
  void nextStateDefesa1() { setNextState(stateDefesa1); }
  private void setNextState(State state) {
    if (log != null) {
      log.estadoMudou(this.state, state);
    }
    this.state = state;
  }  
  
  //SE HOUVER LIMBO JUNTA E BARALHA JOGO
  void baralhar() {
    if (!pilhaLimbo.isEmpty()) {
      doBaralharLimbo();
      pilhaJogo.addAll(pilhaLimbo);
      pilhaLimbo.clear();
      pilhaJogo.baralhar();
      doBaralhar();
    }
  }

  //SIMBOLO != DO ULTIMO JOGADO = JOGADA VALIDA
  public boolean jogadaValida(Carta carta) {
    //Para ajudar na batota
    //se crata != Labirinto é sempre jogada valida
    if( !(carta instanceof CartaLabirinto) ){
      return true;
    }
    
    if (pilhaCartasJogadas.isEmpty()) {
      return true;
    }
    else {
      Carta ultimaJogada = pilhaCartasJogadas.get(pilhaCartasJogadas.size() - 1);
      if (carta.getSimbolo().equals(ultimaJogada.getSimbolo())) {
        return false;
      }
      return true;
    }
  }

  //3 CARTAS LABIRINTO CONSECUTIVAS DA MESMA COR
  public boolean coresConsecutivas() {
    int ncartas=4;
    if (pilhaCartasJogadas.size() < ncartas) {
      return false;
    }

    int ultima = pilhaCartasJogadas.size() - 1;
    int coresIguais = 1;
    Cores ultimaCor = pilhaCartasJogadas.get(ultima).getCor();

    for (int i = ultima - 1; i > ultima - ncartas; i--) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        return false;
      }
      if (pilhaCartasJogadas.get(i).getCor().compareTo(ultimaCor) == 0) {
        coresIguais++;
      }
    }
    if (coresIguais == ncartas) {
      return true;
    }
    return false;
  }

  //ESTAO 8 PORTAS NA PILHA DE CARTAS JOGADAS ?
  public boolean encontrouTodasPortas() {
    int totalPortas = 8;
    int portasJogadas = 0;

    for (int i = 0; i < pilhaCartasJogadas.size(); i++) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        portasJogadas++;
      }
    }
    if (portasJogadas == totalPortas) {
      return true;
    }
    return false;
  }

  //INICIA MAO DE 5 CARTAS SEM EFEITO DE COMPRA
  public void iniciaMao() {
    doIniciaMao();
    do {
      Carta c = pilhaJogo.tiraUltimaCarta();
      if (c instanceof CartaLabirinto) {
        mao.add(c);
      }
      else {
        pilhaLimbo.add(c);
      }
    } while (mao.size() != 5);
  } 

  //I STATE JOGAR
  public void joga(Carta carta) {
    state.joga(carta);
  }

  //I STATE JOGAR
  public void descarta(Carta carta) {
    state.descarta(carta);
  }

  //STATE GANHOU executaEstado()
  public void ganhou() {
    emJogo = false;
  }

  //STATE PERDEU executaEstado()
  public void perdeu() {
    emJogo = false;
  }
  
  public void comprou(Carta carta) {
    state.comprou(carta);
  } 

  //############################################################################
  //## LOG DO JOGO
  //##
  void doJogadaInvalida(Carta carta) {
    if (log != null) {
      log.jogadaInvalida(carta);
    }
  }

  void doEncontrouPorta(Carta carta) {
    if (log != null) {
      log.encontrouPorta(carta);
    }
  }

  void doNaoExisteChaveNaMao() {
    if (log != null) {
      log.naoExisteChaveNaMao();
    }
  }

  void doNaoExistePortaNoJogo() {
    if (log != null) {
      log.NaoExistePortaNoJogo();
    }
  }

  void doBaralharLimbo() {
    if (log != null) {
      log.baralharLimbo();
    }
  }

  void doBaralhar() {
    if (log != null) {
      log.baralhar();
    }
  }

  void doIniciaMao() {
    if (log != null) {
      log.iniciaMao();
    }
  }

  void doMoveCarta(Cartas origem, Carta carta, Cartas destino) {
    if (log != null) {
      log.moveCarta(origem, carta, destino);
    }
  }

  void doGanhouJogo() {
    if (log != null) {
      log.ganhouJogo();
    }
  }

  void doPerdeuJogo() {
    if (log != null) {
      log.perdeuJogo();
    }
  }

  void doCartaSonhoDescartada() {
    if (log != null) {
      log.cartaSonhoDescartada();
    }
  }

  void doCartaPortaDescartada(CartaPorta carta) {
    if (log != null) {
      log.cartaPortaDescartada(carta);
    }
  }

  void doCartaLabirintoDescartada(CartaLabirinto carta) {
    if (log != null) {
      log.cartaLabirintoDescartada(carta);
    }
  }

  void doNaoExistemCartasSuficientes(int nPesadelo) {
    if (log != null) {
      log.naoExistemCartasSuficientes(nPesadelo);
    }
  }
  
}