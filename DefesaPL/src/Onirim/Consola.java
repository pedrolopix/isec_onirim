package Onirim;

import java.util.Scanner;

public class Consola {

  private Onirim onirim;

  //CONSTRUTOR
  public Consola(Onirim onirim) {
    this.onirim = onirim;
  }

  //INICIO DA ACCAO
  public void run() {
    int op;
    do {
      System.out.println("===================================================");
      System.out.println("====================   ONIRIM   ===================");
      System.out.println("===================================================");
      System.out.println("1. Jogar");
      System.out.println("2. Terminar");
      System.out.println("");
      System.out.print("Opção: ");
      op = lerInt();
      if (op == 1) {
        jogo();
      }
    } while (op != 2);
  }

  private void jogo() {
    onirim.getStateJogar().setIExecute(new menuJogo());
    onirim.getStateComprouPorta().setIExecute(new menuComprouPorta());
    onirim.getStateComprouSonho().setIExecute(new menuComprouSonho());
    onirim.getStateProfecia().setIExecute(new menuProfecia());
    onirim.getStateDefesa1().setIExecute(new menuDefesa1());

    //StateIniciarJogo que prepara cartas
    onirim.iniciarJogo();

    while (onirim.getEmJogo()) {
      onirim.executaEstado();
    }
  }

  //############################################################################
  //## Menus - Implementação dos Interfaces
  //##
  
  //STATE JOGAR
  class menuJogo implements IExecute {

    @Override
    public void executar(State state) {
      Carta carta;
      int opcao = 0;

      System.out.println("\n");
      System.out.println("===================================================");
      mostraCartasJogadas();
      mostraMao();
      System.out.println("===================================================");
      String[] opcoes = {"Jogar", "Descartar", "Ver 7 cartas",
                         "Acrescentar na Mão", "Acrescentar ao Baralho",
                         "Acrescentar a Mesa", "Mostrar restantes cartas",
                         "Terminar"};
      do {
        for (int i = 0; i < opcoes.length; i++) {
          System.out.println((i + 1) + ". " + opcoes[i]);
        }
        System.out.print("Opção: ");
        opcao = lerInt();
      } while (opcao < 1 || opcao > 8);

      switch (opcao) {
        case 1:
          carta = escolheCarta("Carta para jogar", onirim.getMao(), -1);
          if (carta != null) {
            onirim.joga(carta);
          }
          break;
        case 2:
          carta = escolheCarta("Carta para descartar", onirim.getMao(), -1);
          if (carta != null) {
            onirim.descarta(carta);
          }
          break;
        case 3:
          mostrar7CartasJogo();
          break;
        case 4:
          carta = escolheCarta("Carta para colocar na mao", onirim.getPilhaJogo(), -1);
          //cartaMao = escolheCarta("Qual pretende substituir?", onirim.getMao(), -1);
          //onirim.getMao().moverCartaPara(cartaMao, onirim.getPilhaLimbo());
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getMao());
          break;
        case 5:
          carta = escolheCarta("Carta para o topo do baralho", onirim.getPilhaJogo(), -1);
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getPilhaJogo());
          break;
        case 6:
          carta = escolheCarta("Carta para a mesa de jogo", onirim.getPilhaJogo(), -1);
          onirim.getPilhaJogo().moverCartaPara(carta, onirim.getPilhaCartasJogadas());
          break;
        case 7:
          mostraCartasJogo();
          mostraCartasDescarte();
          mostraCartasLimbo();
          break;
        case 8:
          //onirim.perdeu();
          onirim.nextStatePerdeu();
          return;
      }
    }
  }

  //STATE COMPROU PORTA
  class menuComprouPorta implements IExecute {

    @Override
    public void executar(State state) {
      Carta cartaPorta = onirim.getPilhaJogo().getUltima();
      Carta cartaChave = onirim.getMao().temChave(cartaPorta.getCor());
      int opcao = 0;

      System.out.println("===================================================");
      System.out.println(" COMPROU PORTA");
      System.out.println("===================================================");
      System.out.println("Acabou de comprar uma porta " + cartaChave.getCor());
      String[] opcoes = {"Descartar chave e colocar porta em jogo ?",
                         "Colocar porta comprada no Limbo ?"};
      do {
        for (int i = 0; i < opcoes.length; i++) {
          System.out.println((i + 1) + ". " + opcoes[i]);
        }
        System.out.print("Opção: ");
        opcao = lerInt();
      } while (opcao < 1 || opcao > 2);

      switch (opcao) {
        case 1:
          onirim.getMao().moverCartaPara(cartaChave, onirim.getPilhaDescarte());
          onirim.getPilhaJogo().moverCartaPara(cartaPorta, onirim.getPilhaCartasJogadas());
          if (onirim.encontrouTodasPortas()) {
            //onirim.ganhou();
            onirim.nextStateGanhou();
            return;
          }
          onirim.nextStateComprar();
          break;
        case 2:
          onirim.getPilhaJogo().moverCartaPara(cartaPorta, onirim.getPilhaLimbo());
          onirim.nextStateComprar();
          break;
      }
    }
  }

  //STATE COMPROU SONHO
  class menuComprouSonho implements IExecute {

    @Override
    public void executar(State state) {
      StateComprouSonho sonho = (StateComprouSonho) state;
      int opcao = 0;

      System.out.println("===================================================");
      System.out.println(" COMPROU PESADELO");
      System.out.println("===================================================");
      String[] opcoes = {"Descartar da mão uma Carta Chave.",
                         "Enviar uma Carta Porta ja jogada para o Limbo.",
                         "Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.",
                         "Descartar Mão e comprar nova, tal como inicialmente."};
      do {
        for (int i = 0; i < opcoes.length; i++) {
          System.out.println((i + 1) + ". " + opcoes[i]);
        }
        System.out.print("Opção: ");
        opcao = lerInt();
      } while (opcao < 1 || opcao > 5);

      switch (opcao) {
        case 1:
          sonho.executaPesadeloUm();
          break;
        case 2:
          sonho.executaPesadeloDois();
          break;
        case 3:
          sonho.executaPesadeloTres();
          break;
        case 4:
          sonho.executaPesadeloQuatro();
          break;
      }
    }
  }

  //STATE PROFECIA (3 cores iguais consecutivas)
  class menuProfecia implements IExecute {

    @Override
    public void executar(State state) {
      Carta carta;
      Cartas pilhaAuxiliar = new Cartas(onirim, "");

      System.out.println("===================================================");
      System.out.println(" PROFECIA");
      System.out.println("===================================================");
      
      for (int i = 0; i < 5; i++) {
        carta = onirim.getPilhaJogo().tiraUltimaCarta();
        pilhaAuxiliar.add(carta);
      }

      System.out.println("Cartas da Profecia" + pilhaAuxiliar);
      carta = escolheCarta("Qual pretende descartar", pilhaAuxiliar, -1);
      pilhaAuxiliar.moverCartaPara(carta, onirim.getPilhaDescarte());
      System.out.println("__________________________________________");
      System.out.println("Organizar as cartas de volta para o jogo");
      while (!pilhaAuxiliar.isEmpty()) {
        System.out.println("__________________________________________");
        System.out.println("Cartas da Profecia" + pilhaAuxiliar);
        carta = escolheCarta("Carta", pilhaAuxiliar, -1);
        pilhaAuxiliar.moverCartaPara(carta, onirim.getPilhaJogo());
      }
      onirim.nextStateComprar();
    }
  }

  class menuDefesa1 implements IExecute {

    @Override
    public void executar(State state) {
      System.out.println("=========================================================================");
      System.out.println(" DEFESA1 - Devolver um SOL da pilha de descarte para o topo do baralho");
      System.out.println("=========================================================================");
      
      if (onirim.getPilhaDescarte().isEmpty())
      {
        System.out.println("Não há cartas da pilha de descarte!");
        return;
      }
      
      //existe carta com sol?
      if (onirim.getPilhaDescarte().temSol()==null) {
        System.out.println("Não há cartas com o simbolo SOL da pilha de descarte!");
        return;
      }
      
      //mostrar descarte
      System.out.println("Pilha de descarte: " + onirim.getPilhaDescarte());
      Carta carta;
      do {
         carta= escolheCarta("Qual sol?", onirim.getPilhaDescarte(), -1);
         
      } while (carta.getSimbolo()!= Simbolos.sol);
      
      onirim.getPilhaDescarte().moverCartaPara(carta, onirim.getPilhaJogo());
      
    }
    
  }
  
  
  //############################################################################
  //## FUNÇÕES AUXILIARES
  //##
  public void mostraMao() {
    System.out.println(onirim.getMao());
  }

  public void mostraCartasJogo() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaJogo());
  }
  
  public void mostraCartasJogadas() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaCartasJogadas());
  }
  
  public void mostraCartasLimbo() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaLimbo());
  }

  public void mostraCartasDescarte() {
    System.out.print("Cartas ");
    System.out.println(onirim.getPilhaDescarte());
  }

  public void mostrar7CartasJogo() {
    System.out.print("Ultimas 7 cartas de jogo");
    System.out.println(onirim.getPilhaJogo().getCartasJogo(7));
  }

  //PEDE NOME DA CARTA DE UMA DADA PILHA DE CARTAS
  Carta escolheCarta(String accao, Cartas cartas, int nCartasFim) {
    String str;
    Carta carta = null;
    do {
      System.out.print(accao + ": ");
      str = lerString();
      carta = cartas.getCarta(str, nCartasFim);
      if (carta == null) {
        System.out.println("A carta " + str + " não existe, escolha outra.");
      }
    } while (carta == null);
    return carta;
  }

  private String lerString() {
    Scanner sc = new Scanner(System.in);
    return sc.nextLine();
  }

  private int lerInt() {
    Scanner s = new Scanner(System.in);
    while (!s.hasNextInt()) {
      s.next();
    }
    return s.nextInt();
  }
}
