package Onirim;

class StateIniciarJogo extends State {

  public StateIniciarJogo(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    getOnirim().getPilhaJogo().clear();
    getOnirim().getPilhaLimbo().clear();
    getOnirim().getPilhaDescarte().clear();
    getOnirim().getMao().clear();
    getOnirim().getPilhaCartasJogadas().clear();

    //CRIA BARALHO BASE E COLOCA NOS BARALHOD DE JOGO
    getOnirim().getBaralhos().add(new BaralhoBase());

    for (int i = 0; i < getOnirim().getBaralhos().size(); i++) {
      getOnirim().getPilhaJogo().addAll(getOnirim().getBaralhos().get(i).getCartas());
    }

    getOnirim().getPilhaJogo().baralhar();
    getOnirim().iniciaMao();
    getOnirim().baralhar();

    getOnirim().nextStateJogar();
  }

  @Override
  public String toString() {
    return "Novo Jogo";
  }
}
