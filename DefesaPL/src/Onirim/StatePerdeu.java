package Onirim;

public class StatePerdeu extends State {

  public StatePerdeu(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void executaEstado() {
    super.executaEstado();
    getOnirim().perdeu();
    getOnirim().doPerdeuJogo();
    
  }

  @Override
  public String toString() {
    return "Perdeu";
  }
}
