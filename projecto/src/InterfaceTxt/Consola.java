package InterfaceTxt;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.*;
import ExpansaoTorresEstados.StateComprouSonhoTorres;
import ExpansaoTorresEstados.StateDescartarTorreJogada;
import ExpansaoTorresEstados.StateOrganizarTorres;
import ExpansaoTorresEstados.StateTorreDescartadaOrganiza;
import Onirim.Modelo;
import java.util.Scanner;

public class Consola {

  private Modelo modelo;

  //CONSTRUTOR
  public Consola(Modelo modelo) {
    this.modelo = modelo;
  }

  //INICIO DA ACCAO
  public void run() {
    int op;
    do {
      op = menuNovoJogo();
      switch (op) {
        case 0:
          InicioJogoBase();
          break;
        case 1:
          InicioJogoExt1();
          break;
        case 2:
          InicioJogoExt2();
          break;
        case 3:
          InicioJogoExt3();
          break;
      }
    } while (op != 4);
  }

  private void InicioJogoBase() {
    modelo.novoJogoBase();
    iniciarJogo();
  }

  private void InicioJogoExt1() {
    modelo.novoJogoExp1();
    iniciarJogo();
  }

  private void InicioJogoExt2() {
    modelo.novoJogoExp2();
    iniciarJogo();
  }

  private void InicioJogoExt3() {
    System.out.println("Não implementado");
  }

  private void iniciarJogo() {

    while ((modelo.getOnirim() != null) && !(modelo.getState() instanceof StateFinal)) {
      interagirComUtilizador();
    }
  }

  //APRESENTAÇÃO DE MENUS PARA INTERACCAO COM O UTILIZADOR
  public void interagirComUtilizador() {
    State state = modelo.getState();

    //Estados sem necessidade de pedir nada ao utilizador
    //executam automaticamente (onEnter)
    //COMPRAR, COMPROUPORTA, GANHOU, PERDEU, PROFECIA

    if (state instanceof StateComprouPortaSemChave) {
      modelo.comprouPortaSemChave();
    }

    if (state instanceof StateComprouPortaComChave) {
      int opcao = menuStateComprouPorta();
      modelo.comprouPortaComChave(opcao);
    }

    if (state instanceof StateProfeciaDescarta) {
      Carta carta;
      Cartas cartasProfecia = modelo.getPilhaCartasProfecia();
      System.out.println("Cartas da Profecia" + cartasProfecia);
      carta = escolheCarta("Qual pretende descartar", cartasProfecia, -1);
      modelo.descartarCartaProfecia(carta);

    }

    if (state instanceof StateProfeciaOrganiza) {
      Carta carta;
      Cartas cartasProfecia = modelo.getPilhaCartasProfecia();
      System.out.println("Cartas da Profecia" + cartasProfecia);
      carta = escolheCarta("Organizar as cartas de volta para o jogo", cartasProfecia, -1);
      modelo.organizarCartasProfecia(carta);
    }

    if (state instanceof StateComprouSonho) {
      try {
        int opcao = menuStateComprouSonho();
        modelo.comprouSonho(opcao);
      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      }
    }

    if (state instanceof StateFaseJogo) {
      Carta carta;
      int opcao = menuStateFaseJogo();
      switch (opcao) {
        case 1:
          carta = escolheCarta("Carta para jogar", modelo.getMao(), -1);
          if (carta != null) {
            try {
              modelo.jogarCarta(carta);
            } catch (Exception ex) {
              System.out.println(ex.getMessage());
            }
          }
          break;
        case 2:
          carta = escolheCarta("Carta para descartar", modelo.getMao(), -1);
          if (carta != null) {
            modelo.descartarCarta(carta);
          }
          break;
        case 3:
          mostrar7CartasJogo();
          break;
        case 4:
          carta = escolheCarta("Carta para colocar na mao", modelo.getPilhaJogo(), -1);
          //cartaMao = escolheCarta("Qual pretende substituir?", onirim.getMao(), -1);
          //onirim.getMao().moverCartaPara(cartaMao, onirim.getPilhaLimbo());
          modelo.getPilhaJogo().moverCartaPara(carta, modelo.getMao());
          break;
        case 5:
          carta = escolheCarta("Carta para o topo do baralho", modelo.getPilhaJogo(), -1);
          modelo.getPilhaJogo().moverCartaPara(carta, modelo.getPilhaJogo());
          break;
        case 6:
          carta = escolheCarta("Carta para a mesa de jogo", modelo.getPilhaJogo(), -1);
          modelo.getPilhaJogo().moverCartaPara(carta, modelo.getPilhaCartasJogadas());
          break;
        case 7:
          mostraCartasJogo();
          mostraCartasDescarte();
          mostraCartasLimbo();
          break;
        case 8:
          modelo.terminarJogo();
          break;
      }
    }

    if (state instanceof StateOrganizarTorres) {
      try {
        Carta carta;
        Cartas cartasTorre = modelo.getPilhaAuxiliar();
        System.out.println("Cartas Torre" + cartasTorre);
        carta = escolheCarta("Organizar as cartas de volta para a mesa", cartasTorre, -1);
        modelo.organizarTorres(carta, false);
      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      }
      
      
      
      
    }

    if (state instanceof StateTorreDescartadaOrganiza) {
      Carta carta;
      Cartas cartasParaOrganizar = modelo.getPilhaAuxiliar();
      System.out.println("Cartas para Organizar" + cartasParaOrganizar);
      carta = escolheCarta("Organizar as cartas de volta para pilha de jogo", cartasParaOrganizar, -1);
      modelo.torreDescartadaOrganizaCartas(carta);
    }

    if (state instanceof StateComprouSonhoTorres) {
      try {
        int opcao = menuComprouSonhoTorres();
        modelo.comprouSonhoNaExpansaoTorres(opcao);
      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      }
      
    }

    if (state instanceof StateDescartarTorreJogada) {
      Carta carta;
      Cartas cartasTorre = modelo.getPilhaTorres();
      System.out.println("Cartas Torre" + cartasTorre);
      carta = escolheCarta("Carta Torre a descartar ", cartasTorre, -1);
      modelo.descartarTorreJogada(carta);
    }









  }

  //############################################################################
  //## Menus
  //##
  public int menuNovoJogo() {
    int opcao;
    do {
      System.out.println("===================================================");
      System.out.println("====================   ONIRIM   ===================");
      System.out.println("===================================================");
      System.out.println("0. Jogo Base");
      System.out.println("1. Jogo Base + Expansao 1 : Livro dos Passos");
      System.out.println("2. Jogo Base + Expansao 2 : As Torres ");
      System.out.println("3. Jogo Base + Expansao 3 : Premonições Sombrias");
      System.out.println("4. Sair");
      System.out.println("");
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 0 || opcao > 4);
    return opcao;
  }

  public int menuStateFaseJogo() {
    int opcao = 0;
    System.out.println("\n");
    System.out.println("===================================================");
    System.out.println(modelo.getOnirim());
    System.out.println("===================================================");
    mostraCartasJogadas();
    mostraMao();
    System.out.println("===================================================");
    String[] opcoes = {"Jogar",
                       "Descartar",
                       "Ver 7 cartas",
                       "Acrescentar na Mão",
                       "Acrescentar ao Baralho",
                       "Acrescentar a Mesa",
                       "Mostrar restantes cartas",
                       "Terminar"};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > opcoes.length);
    return opcao;
  }

  public int menuStateComprouPorta() {
    int opcao = 0;
    System.out.println("===================================================");
    System.out.println(" COMPROU PORTA");
    System.out.println("===================================================");
    System.out.println("Acabou de comprar uma porta");
    String[] opcoes = {"Descartar chave e colocar porta em jogo ?",
                       "Colocar porta comprada no Limbo ?"};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > opcoes.length);
    return opcao;
  }

  public int menuStateComprouSonho() {
    int opcao = 0;
    System.out.println("===================================================");
    System.out.println(" COMPROU PESADELO");
    System.out.println("===================================================");
    String[] opcoes = {"Descartar da mão uma Carta Chave.",
                       "Enviar uma Carta Porta ja jogada para o Limbo.",
                       "Proximas 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.",
                       "Descartar Mão e comprar nova, tal como inicialmente."};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > opcoes.length);
    return opcao;
  }

  public int menuComprouSonhoTorres() {
    int opcao = 0;
    System.out.println("===================================================");
    System.out.println(" COMPROU PESADELO - EXPANSÃO TORRES");
    System.out.println("===================================================");
    String[] opcoes = {"Descartar uma torre ja jogada.",
                       "Colocar Pesadelo de volta no Limbo."};
    do {
      for (int i = 0; i < opcoes.length; i++) {
        System.out.println((i + 1) + ". " + opcoes[i]);
      }
      System.out.print("Opção: ");
      opcao = lerInt();
    } while (opcao < 1 || opcao > opcoes.length);
    return opcao;
  }

  //############################################################################
  //## FUNÇÕES AUXILIARES
  //##
  public void mostraMao() {
    System.out.println(modelo.getMao());
  }

  public void mostraCartasJogo() {
    System.out.print("Cartas ");
    System.out.println(modelo.getPilhaJogo());
  }

  public void mostraCartasJogadas() {
    System.out.print("Cartas ");
    System.out.println(modelo.getPilhaCartasJogadas());
  }

  public void mostraCartasLimbo() {
    System.out.print("Cartas ");
    System.out.println(modelo.getPilhaLimbo());
  }

  public void mostraCartasDescarte() {
    System.out.print("Cartas ");
    System.out.println(modelo.getPilhaDescarte());
  }

  public void mostrar7CartasJogo() {
    System.out.print("Ultimas 7 cartas de jogo");
    System.out.println(modelo.getPilhaJogo().getCartasJogo(7));
  }

  //PEDE NOME DA CARTA DE UMA DADA PILHA DE CARTAS
  Carta escolheCarta(String accao, Cartas cartas, int nCartasFim) {
    String str;
    Carta carta = null;
    do {
      System.out.print(accao + ": ");
      str = lerString();
      carta = cartas.getCarta(str, nCartasFim);
      if (carta == null) {
        System.out.println("A carta " + str + " não existe, escolha outra.");
      }
    } while (carta == null);
    return carta;
  }

  private String lerString() {
    Scanner sc = new Scanner(System.in);
    return sc.nextLine();
  }

  private int lerInt() {
    Scanner s = new Scanner(System.in);
    while (!s.hasNextInt()) {
      s.next();
    }
    return s.nextInt();
  }
}
