package Onirim;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseCartas.Cores;
import BaseCartas.Simbolos;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;
import ExpansaoTorresCartas.CartaTorre;
import InterfaceTxt.ConsolaLog;
import java.io.*;
import java.util.ArrayList;
import java.util.Observable;

public class Modelo extends Observable {

  Onirim onirim;
  private ConsolaLog consolaLog;

  public void novoJogoBase() {
    onirim = null;
    setChanged();
    notifyObservers();

    onirim = new Onirim();
    onirim.setLog(consolaLog);
    onirim.iniciarJogo();
    setChanged();
    notifyObservers();
  }

  public void novoJogoExp1() {
    onirim = null;
    setChanged();
    notifyObservers();
  }

  public void novoJogoExp2() {
    onirim = null;
    setChanged();
    notifyObservers();

    onirim = new OnirimExp2();
    onirim.setLog(consolaLog);
    onirim.iniciarJogo();
    setChanged();
    notifyObservers();
  }

  //############################################################################
  public void comprouPortaSemChave() {
    onirim.comprouPortaSemChave();
    setChanged();
    notifyObservers();
  }

  public void comprouPortaComChave(int opcao) {
    onirim.comprouPortaComChave(opcao);
    setChanged();
    notifyObservers();
  }

  public void descartarCartaProfecia(Carta carta) {
    onirim.descartarCartaProfecia(carta);
    setChanged();
    notifyObservers();
  }

  public void organizarCartasProfecia(Carta carta) {
    onirim.organizarCartasProfecia(carta);
    setChanged();
    notifyObservers();
  }

  public void comprouSonho(int opcao) throws Exception {
    onirim.comprouSonho(opcao);
    setChanged();
    notifyObservers();
  }

  public void jogarCarta(Carta carta) throws Exception {
    onirim.jogarCarta(carta);
    setChanged();
    notifyObservers();
  }

  public void descartarCarta(Carta carta) {
    onirim.descartarCarta(carta);
    setChanged();
    notifyObservers();
  }

  public void terminarJogo() {
    onirim.terminarJogo();
    onirim = null;
    setChanged();
    notifyObservers();
  }

  //############################################################################
  // GET
  public Onirim getOnirim() {
    return onirim;
  }

  public State getState() {
    return onirim.getState();
  }

  public Cartas getPilhaCartasProfecia() {
    return onirim.getPilhaCartasProfecia();
  }

  public Cartas getPilhaJogo() {
    return onirim.getPilhaJogo();
  }

  public Cartas getMao() {
    return onirim.getMao();
  }

  public Cartas getPilhaCartasJogadas() {
    return onirim.getPilhaCartasJogadas();
  }

  public Cartas getPilhaLimbo() {
    return onirim.getPilhaLimbo();
  }

  public Cartas getPilhaDescarte() {
    return onirim.getPilhaDescarte();
  }

  //############################################################################
  // EXPANSAO TORRES  
  public void organizarTorres(Carta carta, Boolean limbo) throws Exception {
    if (onirim instanceof OnirimExp2) {
      ((OnirimExp2) onirim).organizarTorres(carta, limbo);
    }
    setChanged();
    notifyObservers();
  }
  
  public void reOrganizarTorres() {
    if (onirim instanceof OnirimExp2) {
      ((OnirimExp2) onirim).reOrganizarTorres();
    }
    setChanged();
    notifyObservers();
  }

  public void torreDescartadaOrganizaCartas(Carta carta) {
    if (onirim instanceof OnirimExp2) {
      ((OnirimExp2) onirim).torreDescartadaOrganizaCartas(carta);
    }
    setChanged();
    notifyObservers();
  }

  public void comprouSonhoNaExpansaoTorres(int opcao) throws Exception {
    if (onirim instanceof OnirimExp2) {
      ((OnirimExp2) onirim).comprouSonhoNaExpansaoTorres(opcao);
    }
    setChanged();
    notifyObservers();
  }

 
  public void descartarTorreJogada(Carta carta) {
    if (onirim instanceof OnirimExp2) {
      ((OnirimExp2) onirim).descartarTorreJogada(carta);
    }
    setChanged();
    notifyObservers();
  }

  public Cartas getPilhaAuxiliar() {    
    return ((OnirimExp2) onirim).getPilhaAuxiliar();
  }

  public Cartas getPilhaTorres() {
    return ((OnirimExp2) onirim).getPilhaTorres();
  }

  void setLog(ConsolaLog consolaLog) {
    this.consolaLog = consolaLog;
  }

  public void saveToFileName(String filename) {

    ObjectOutputStream out;
    try {
      File f = new File(filename);
      out = new ObjectOutputStream(new FileOutputStream(f, false));
      out.writeObject(onirim);
      out.close();
    } catch (IOException ex) {
      System.out.println("Erro ao gravar ficheiro!!!\n" + ex.getMessage());
    }


  }

  public void lerFromFileName(String fileName) throws IOException, ClassNotFoundException {
    File f = new File(fileName);

    ObjectInputStream in;
    try {
      in = new ObjectInputStream(new FileInputStream(f));
      onirim = (Onirim) in.readObject();
      onirim.setLog(consolaLog);
      in.close();
      setChanged();
      notifyObservers();
    } catch (IOException | ClassNotFoundException ex) {
      throw ex;
    }

  }

  public void setTeste() {
//    
//    ((OnirimExp2)onirim).nextStateOrganizarTorres();
//        
//    ArrayList<Simbolos> luaSol = new ArrayList<>();
//    ArrayList<Simbolos> lua = new ArrayList<>();
//    ArrayList<Simbolos> sol = new ArrayList<>();
//    
//    luaSol.add(Simbolos.lua);
//    luaSol.add(Simbolos.sol);
//    lua.add(Simbolos.lua);
//    sol.add(Simbolos.sol);
//    
//    
//    
//    ((OnirimExp2)onirim).getPilhaTorres().add( new CartaTorre(Cores.azul, lua, lua, 4) );
//    ((OnirimExp2)onirim).getPilhaTorres().add( new CartaTorre(Cores.azul, lua, null, 5) );
//    
//    //TORRES CASTANHAS
//
//    ((OnirimExp2)onirim).getPilhaTorres().add( new CartaTorre(Cores.castanho, lua, sol, 4) );
//    ((OnirimExp2)onirim).getPilhaTorres().add( new CartaTorre(Cores.castanho, null, sol, 5) );  
    
     
    onirim.nextStateComprouPortaComChave();
     setChanged();
    notifyObservers(); 
  }

  
}
