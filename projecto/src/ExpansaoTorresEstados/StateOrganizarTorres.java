package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;
import ExpansaoTorresCartas.CartaTorre;

public class StateOrganizarTorres extends State implements StateTorres {

  //CONSTRUTOR
  public StateOrganizarTorres(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2) super.getOnirim();
  }

  @Override
  public void organizarTorres(Carta carta, Boolean limbo) throws Exception {

    
    
    if( limbo ){
      getOnirim().getPilhaAuxiliar().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      mudarEstado();
      //getOnirim().nextStateOrganizarTorres();
      return;
    }

    if (getOnirim().getPilhaTorres().isEmpty()) {
      getOnirim().getPilhaAuxiliar().moverCartaPara(carta, getOnirim().getPilhaTorres());
      mudarEstado();
      //getOnirim().nextStateOrganizarTorres();
      return;
    }

    if (getOnirim().jogadaValidaTorres(carta)) {
      getOnirim().getPilhaAuxiliar().moverCartaPara(carta, getOnirim().getPilhaTorres());
      mudarEstado();
      //getOnirim().nextStateOrganizarTorres();
    }
    else {
      getOnirim().nextStateOrganizarTorres();
      throw new Exception("Jogada invalida!\nLado esquerdo partilha simbolos com lado direito da carta ja jogada.");
    }
  }
  
  protected void mudarEstado(){
    if (getOnirim().getPilhaAuxiliar().isEmpty()) {
      getOnirim().nextStateComprar();
    }
    else{
      getOnirim().nextStateOrganizarTorres();
    }
  }

  @Override
  public void reOrganizarTorres() {
    int quantidade = getOnirim().getPilhaTorres().size();
    for(int i=0; i<quantidade; i++){
      CartaTorre torre = (CartaTorre) getOnirim().getPilhaTorres().tiraUltimaCarta();
      getOnirim().getPilhaAuxiliar().add(torre);
    }
    getOnirim().nextStateOrganizarTorres();
  }

  @Override
  public String toString() {
    return "Organizar Torres";
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
  }
}
