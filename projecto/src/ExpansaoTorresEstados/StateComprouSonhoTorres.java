package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;


public class StateComprouSonhoTorres extends State  implements StateTorres{

  //CONSTRUTOR
  public StateComprouSonhoTorres(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) throws Exception  {
    switch(opcao){
      case 1 : descartarTorreJogada(); break;
      case 2 : colocarPesadeloLimbo(); break;      
    }    
  }
    
  public void colocarPesadeloLimbo() {
    //Carta pesadelo = getOnirim().getPilhaDescarte().getUltima();
    Carta pesadelo = getOnirim().getPilhaDescarte().getNextSonho();
    getOnirim().getPilhaDescarte().moverCartaPara(pesadelo, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }

  
  public void descartarTorreJogada() throws Exception {
    if(getOnirim().getPilhaTorres().isEmpty()){
      colocarPesadeloLimbo();
      throw new Exception("Ainda não foi jogada nenhuma Carta Torre para poder descartar!\nSonho vai ser colocado no Limbo");
    }
    getOnirim().nextStateDescartarTorre();    
  }

  @Override
  public String toString() {
    return "Comprou Sonho Torres";
  }

  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
    
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
    
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
   
  }
  
  @Override
  public void reOrganizarTorres() {
   
  }


}
