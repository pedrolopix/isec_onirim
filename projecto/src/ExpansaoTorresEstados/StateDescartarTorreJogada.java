package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;

public class StateDescartarTorreJogada extends State implements StateTorres{

  //CONSTRUTOR
  public StateDescartarTorreJogada(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
        
    getOnirim().getPilhaTorres().moverCartaPara(carta, getOnirim().getPilhaDescarte());
    
    if( getOnirim().getPilhaTorres().isEmpty() || getOnirim().getPilhaTorres().size() == 1){
      getOnirim().nextStateComprar();
      return;
    }
    
    
    
    getOnirim().getPilhaAuxiliar().clear();
    getOnirim().getPilhaAuxiliar().addAll( getOnirim().getPilhaTorres() );
    getOnirim().getPilhaTorres().clear();    
    
    getOnirim().nextStateOrganizarTorres();         
  }

  @Override
  public String toString() {
    return "Descartar Torre";
  }

  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
   
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
   
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
    
  }
  
  @Override
  public void reOrganizarTorres() {
   
  }
  
  
  
  
}
