package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.CartaPorta;
import BaseCartas.CartaSonho;
import ExpansaoTorres.OnirimExp2;

public class StateComprouSonhoExp2 extends BaseEstados.StateComprouSonho implements StateTorres{

  //CONSTRUTOR
  public StateComprouSonhoExp2(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2) super.getOnirim();
  }

  @Override
  public void comprouSonho(int opcao) throws Exception {
    super.comprouSonho(opcao);
  }

  //1 - Descartar da mao uma Carta Chave.
  @Override
  public void executaPesadeloUm() throws Exception {
    Carta carta = getOnirim().getMao().temChave();
    if (carta == null) {
      getOnirim().doNaoExisteChaveNaMao();
      getOnirim().nextStateComprouSonho();
      throw new Exception("Não tem nenhuma chave na mão para poder descartar!");
    }
    getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaDescarte());
    comprouSonhoNaExpansaoTorres();
  }

  //2 - Enviar uma Carta Porta ja jogada para o Limbo.
  @Override
  public void executaPesadeloDois() throws Exception {
    Carta carta = getOnirim().getPilhaCartasJogadas().getNextPorta();
    if (carta == null) {
      getOnirim().doNaoExistePortaNoJogo();
      getOnirim().nextStateComprouSonho();
      throw new Exception("Ainda não foi jogada uma carta Porta para que possa enviar para o Limbo!");
    }
    getOnirim().getPilhaCartasJogadas().moverCartaPara(carta, getOnirim().getPilhaLimbo());
    comprouSonhoNaExpansaoTorres();
  }

  //3 - Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.
  @Override
  public void executaPesadeloTres() throws Exception {

    int ver = 5;

    if (getOnirim().getPilhaJogo().isEmpty()) {
      getOnirim().nextStatePerdeu();
      throw new Exception("Nao tem mais cartas na pilha de jogo. Perdeu!");
    }

    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(3);
      getOnirim().nextStateComprouSonho();
      throw new Exception("Nao tem cartas suficientes na pilha de jogo!");
    }

    int ultima = getOnirim().getPilhaJogo().size() - 1;
    for (int i = ultima; i > ultima - ver && i > 0; i--) {
      Carta carta = getOnirim().getPilhaJogo().get(i);
      if ((carta instanceof CartaSonho) || (carta instanceof CartaPorta)) {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      }
      else {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      }
    }
    comprouSonhoNaExpansaoTorres();
  }

  //4 - Descartar Mao e comprar nova, tal como inicialmente.
  @Override
  public void executaPesadeloQuatro() throws Exception {
    if (getOnirim().getPilhaJogo().temLabirintos() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(4);
      getOnirim().nextStateComprouSonho();
      throw new Exception("Nao tem cartas Labirinto suficientes na pilha de jogo!");
    }
    getOnirim().getPilhaDescarte().addAll(getOnirim().getMao());
    getOnirim().getMao().clear();
    getOnirim().iniciaMao();
    comprouSonhoNaExpansaoTorres();
  }

  private void comprouSonhoNaExpansaoTorres() {
    if (getOnirim().getPilhaTorres().size() == getOnirim().getMaxTorres()) {
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().nextStateComprouSonhoTorres();
  }

//  @Override
//  public String toString() {
//    return "Comprou Sonho";
//  }

  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
    
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
    
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
    
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
    
  }
  
  @Override
  public void reOrganizarTorres() {
   
  }
  
}
