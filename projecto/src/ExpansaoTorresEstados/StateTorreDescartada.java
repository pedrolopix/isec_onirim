package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;
import ExpansaoTorresCartas.CartaTorre;

public class StateTorreDescartada extends State implements StateTorres{

  //CONSTRUTOR
  public StateTorreDescartada(Onirim onirim) {
    super(onirim);
  }

  
  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

 
  @Override
  public void onEnter(){
    Carta cartaTorre;
    
    cartaTorre = getOnirim().getPilhaDescarte().getUltima();  
    int quantidade = ((CartaTorre)cartaTorre).getValor();
    
    if(getOnirim().getPilhaJogo().size() < quantidade){
      getOnirim().nextStateComprar();
      /*
      throw new Exception("Nao tem cartas suficientes no Baralho para a profecia das Torres\n"
              + "(Ver cartas e repor no Baralho)\n\n"
              + "Carta sera simplesmente descartada");              
      */
    }
    
    getOnirim().getPilhaAuxiliar().clear();    
    for(int i=0; i<quantidade; i++){      
      Carta carta = getOnirim().getPilhaJogo().getUltima();
      getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaAuxiliar());      
      //Carta carta = getOnirim().getPilhaJogo().tiraUltimaCarta();
      //getOnirim().getPilhaAuxiliar().add(carta);
    }    
    getOnirim().nextStateTorreDescartadaOrganiza();
  }
  
  
  @Override
  public String toString() {
    return "Torre Descartada";
  } 
  
    
  
  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
   
  }


  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
    
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
   
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
    
  }
  
  @Override
  public void reOrganizarTorres() {
   
  }
  
  
}
