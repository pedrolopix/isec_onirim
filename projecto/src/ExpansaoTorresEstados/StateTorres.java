package ExpansaoTorresEstados;

import BaseCartas.Carta;

public interface StateTorres {

  //******* EXPANSAO TORRES  
  public void organizarTorres(Carta carta, Boolean limbo) throws Exception;

  public void torreDescartadaOrganizaCartas(Carta carta);

  public void descartarTorreJogada(Carta carta);

  public void comprouSonhoNaExpansaoTorres(int opcao) throws Exception;

  public void reOrganizarTorres();
  
  //public void descartarTorreJogada() {}
  //public void colocarPesadeloLimbo() {}
}
