
package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseEstados.State;
import ExpansaoTorres.OnirimExp2;


public class StateTorreDescartadaOrganiza extends State implements StateTorres{

  //CONSTRUTOR
  public StateTorreDescartadaOrganiza(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

  
 
  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {    
    getOnirim().getPilhaAuxiliar().moverCartaPara(carta, getOnirim().getPilhaJogo());
      
    if(getOnirim().getPilhaAuxiliar().isEmpty()){
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().nextStateTorreDescartadaOrganiza();
  } 
  
  
  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
    
  }

 
  @Override
  public void descartarTorreJogada(Carta carta) {
    
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
    
  }
  
  @Override
  public void reOrganizarTorres() {
   
  }
  
  @Override
  public String toString() {
    return "Torre Descartada Organiza";
  } 
  
}
