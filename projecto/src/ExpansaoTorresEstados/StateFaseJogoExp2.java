package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import ExpansaoTorres.OnirimExp2;
import ExpansaoTorresCartas.CartaTorre;

public class StateFaseJogoExp2 extends BaseEstados.StateFaseJogo{

  //CONSTRUTOR
  public StateFaseJogoExp2(Onirim onirim) {
    super(onirim);
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }
  
  
  @Override
  public void jogarCarta(Carta carta) throws Exception{
    int maxTorres = 4;
    
    if (carta == null) {
      return;
    }
    
    if(carta instanceof CartaTorre){
      //atingido o maximo de torres necessarias
      if(getOnirim().getPilhaTorres().size() == maxTorres){
        getOnirim().nextStateFaseJogo();
        throw new Exception("Jogada invalida!\nJa se encontram na mesa uma Carta Torre de cada cor.");
      }
      
      //ja esta jogada uma torre da mesma cor
      if(getOnirim().torreIgualJaJogada(carta)){
        getOnirim().nextStateFaseJogo();
        throw new Exception("Jogada invalida!\nJa foi jogada uma Carta Torre dessa cor.");
      }
      
      if(getOnirim().getPilhaTorres().isEmpty()){
        getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaTorres());
        getOnirim().nextStateComprar();
        return;
      }
      
      getOnirim().getPilhaAuxiliar().clear();
      getOnirim().getPilhaAuxiliar().addAll( getOnirim().getPilhaTorres() );      
      getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaAuxiliar());
      getOnirim().getPilhaTorres().clear();

      getOnirim().nextStateOrganizarTorres();
      return;
    }

    
    if (!getOnirim().jogadaValida(carta)) {
      getOnirim().doJogadaInvalida(carta);
      throw new Exception("Jogada Invalida. \nPartilha o mesmo simbolo com a ultima carta jogada!");
    }      

    //efectiva a jogada movendo da mao para as cartas jogadas
    getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaCartasJogadas());
    
    //verificar sequencia de 3 cartas labirinto da mesma cor
    if (getOnirim().coresConsecutivas()) {
      encontrouPorta(carta);
      return;
    }    
    
    //se 8 portas jogadas, ganhou!
    if (getOnirim().encontrouTodasPortas() && getOnirim().encontrouTodasTorres()) {
      getOnirim().nextStateGanhou();
      return;
    }
    
    getOnirim().nextStateComprar();
  }

  
  
  
  
  @Override
  public void encontrouPorta(Carta carta) {
    super.encontrouPorta(carta);
  }
  
  @Override
  public void descartarCarta(Carta carta) {
    super.descartarCarta(carta);
  }

  @Override
  public void terminarJogo() {
    super.terminarJogo();
  }

  @Override
  public String toString() {
    return "Fase Jogo";
  }

  
  
  
  
  
  
  
  
  
}
