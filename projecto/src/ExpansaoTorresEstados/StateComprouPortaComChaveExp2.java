package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import ExpansaoTorres.OnirimExp2;

public class StateComprouPortaComChaveExp2  extends BaseEstados.StateComprouPortaComChave  implements StateTorres{

  //CONSTRUTOR
  public StateComprouPortaComChaveExp2(Onirim onirim) {
    super(onirim);
  }
  
  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }
  

  @Override
  public void comprouPortaComChave(int opcao) {
    super.comprouPortaComChave(opcao);
  }
  
  
  @Override
  public void descartarChaveColocarPortaJogo(Carta cartaPorta, Carta cartaChave) {
    getOnirim().getMao().moverCartaPara(cartaChave, getOnirim().getPilhaDescarte());
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaCartasJogadas());
    if (getOnirim().encontrouTodasPortas() && getOnirim().encontrouTodasTorres()) {
      getOnirim().nextStateGanhou();
    }
    else{
      getOnirim().nextStateComprar();
    }
  }

  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
 
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
    
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
   
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
   
  }

  @Override
  public void reOrganizarTorres() {
   
  }
    
}
