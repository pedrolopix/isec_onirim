package ExpansaoTorresEstados;

import Base.Onirim;
import BaseCartas.Carta;
import ExpansaoTorres.OnirimExp2;
import ExpansaoTorresCartas.BaralhoTorres;

public class StateIniciarJogoExp2 extends BaseEstados.StateIniciarJogo implements StateTorres{

  //CONSTRUTOR
  public StateIniciarJogoExp2(Onirim onirim) {
    super(onirim);
  }
  
  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

  @Override
  protected void preparaJogo() {
    super.preparaJogo();
    
    //CRIA BARALHO DAS TORRES E COLOCA NOS BARALHOS DE JOGO
    getOnirim().getBaralhos().add(new BaralhoTorres());  
    
  }

  @Override
  public void sairDoJogo() {
    super.sairDoJogo();
  }

  @Override
  public String toString() {
    return super.toString();
  }  
  
    
  
  @Override
  public void organizarTorres(Carta carta, Boolean limbo) {
   
  }

  @Override
  public void torreDescartadaOrganizaCartas(Carta carta) {
   
  }

  @Override
  public void comprouSonhoNaExpansaoTorres(int opcao) {
    
  }

  @Override
  public void descartarTorreJogada(Carta carta) {
   
  }

  @Override
  public void reOrganizarTorres() {
    
  }
  
  
  
}
