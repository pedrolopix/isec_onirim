package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.CartaCor;

public class StateFaseJogo extends State {

  public StateFaseJogo(Onirim onirim) {
    super(onirim);
  }
  
  @Override
  public void jogarCarta(Carta carta) throws Exception{
    if (carta == null) {
      return;
    }

    if (!getOnirim().jogadaValida(carta)) {
      getOnirim().doJogadaInvalida(carta);
      throw new Exception("Jogada Invalida. \nPartilha o mesmo simbolo com a ultima carta jogada!");
    }

    //efectiva a jogada movendo da mao para as cartas jogadas
    getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaCartasJogadas());
    
    //verificar sequencia de 3 cartas labirinto da mesma cor
    if (getOnirim().coresConsecutivas()) {
      encontrouPorta(carta);
      return;
    }

    //se 8 portas jogadas, ganhou! (BATOTA - carta n se jogarCarta da mao) 
    if (getOnirim().encontrouTodasPortas()) {
      getOnirim().nextStateGanhou();
      return;
    }
    
    getOnirim().nextStateComprar();
  }

  //3 CARTAS LABIRINTO CONSECUTIVAS DA MESMA COR
  public void encontrouPorta(Carta carta) {
    getOnirim().doEncontrouPorta(carta);
    CartaCor cartaPorta=(CartaCor)carta;
    Carta porta = getOnirim().getPilhaJogo().getNextPorta(cartaPorta.getCor());
    //se não há porta da mesma cor
    if (porta == null) {
      getOnirim().nextStateComprar();
      return;
    }   

    getOnirim().getPilhaJogo().moverCartaPara(porta, getOnirim().getPilhaCartasJogadas());

    //se 8 portas em jogadas, ganhou!
    if (getOnirim().encontrouTodasPortas()) {
      //getOnirim().ganhou();
      getOnirim().nextStateGanhou();
      return;
    }

    getOnirim().nextStateComprar();
  }

  @Override
  public void descartarCarta(Carta carta) {
    carta.foiDescartada(getOnirim());
  }
    
  @Override
  public void terminarJogo(){
    getOnirim().nextStateFinal();
  }

  @Override
  public String toString() {
    return "Fase Jogo";
  }
}
