package BaseEstados;

import BaseCartas.Carta;
import Base.Onirim;
import BaseCartas.CartaPorta;

public class StateComprouPortaComChave extends State{

  public StateComprouPortaComChave(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void comprouPortaComChave(int opcao) {    
    CartaPorta cartaPorta=null;
    Carta carta = (getOnirim().getPilhaJogo().getUltima());
    if (carta instanceof CartaPorta) {
       cartaPorta = (CartaPorta)carta;
    } else {
      throw new UnsupportedOperationException("Not yet implemented");
    }
    
    
    
    Carta cartaChave = getOnirim().getMao().temChave(cartaPorta.getCor());    
    switch(opcao){
      case 1 : descartarChaveColocarPortaJogo(cartaPorta, cartaChave); break;
      case 2 : colocarPortaLimbo(cartaPorta); break;
    }
  } 
  
  public void descartarChaveColocarPortaJogo(Carta cartaPorta, Carta cartaChave) {
    getOnirim().getMao().moverCartaPara(cartaChave, getOnirim().getPilhaDescarte());
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaCartasJogadas());
    if (getOnirim().encontrouTodasPortas()) {
      getOnirim().nextStateGanhou();
    }
    else{
      getOnirim().nextStateComprar();
    }
  }
  
  private void colocarPortaLimbo(Carta cartaPorta) {
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }  

  @Override
  public String toString() {
    return "Comprou Porta Com Chave";
  }

  

  
  
  
  
}
