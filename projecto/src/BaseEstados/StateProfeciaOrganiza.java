package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;

public class StateProfeciaOrganiza extends State{

  public StateProfeciaOrganiza(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void organizarCartasProfecia(Carta carta) {
    getOnirim().getPilhaCartasProfecia().moverCartaPara(carta, getOnirim().getPilhaJogo());
    
    if(getOnirim().getPilhaCartasProfecia().isEmpty()){
      getOnirim().nextStateComprar();
      return;
    }
    getOnirim().nextStateProfeciaOrganiza();    
  } 
  
  
  @Override
  public String toString() {
    return "Profecia Organiza";
  }
}
