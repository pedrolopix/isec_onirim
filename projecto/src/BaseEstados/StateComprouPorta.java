package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.CartaPorta;

public class StateComprouPorta extends State {

  //CONSTRUTOR
  public StateComprouPorta(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void onEnter() {
    CartaPorta cartaPorta=null;
    Carta carta = (getOnirim().getPilhaJogo().getUltima());
    if (carta instanceof CartaPorta) {
       cartaPorta = (CartaPorta)carta;
    } else {
      throw new UnsupportedOperationException("Not yet implemented");
    }
      
    Carta cartaChave = getOnirim().getMao().temChave(cartaPorta.getCor());
    
    if (cartaChave == null) {
       getOnirim().nextStateComprouPortaSemChave();
       return;
    }
    
    getOnirim().nextStateComprouPortaComChave();   
  }
  
  @Override
  public String toString() {
    return "Comprou Porta";
  }


  
}
