package BaseEstados;

import Base.Onirim;

public class StateFinal extends State {

  //CONSTRUTOR
  public StateFinal(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void onEnter() {
    getOnirim().doTerminou();
  } 

  @Override
  public String toString() {
    return "Terminou Jogo";
  }
  
}
