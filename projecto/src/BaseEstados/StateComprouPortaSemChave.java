package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;

public class StateComprouPortaSemChave extends State{

  public StateComprouPortaSemChave(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void comprouPortaSemChave() {
    Carta cartaPorta = getOnirim().getPilhaJogo().getUltima();
    getOnirim().getPilhaJogo().moverCartaPara(cartaPorta, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }
  
  @Override
  public String toString() {
    return "Comprou Porta Sem Chave";
  }
}
