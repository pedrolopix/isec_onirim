package BaseEstados;

import Base.Onirim;

public class StatePerdeu extends State {

  public StatePerdeu(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void onEnter() {
    super.onEnter();
    getOnirim().doPerdeuJogo();
    //getOnirim().nextStateFinal();    
  }

   @Override
  public void terminarJogo() {
    getOnirim().nextStateFinal();
  }

  
  @Override
  public String toString() {
    return "Perdeu";
  }
}
