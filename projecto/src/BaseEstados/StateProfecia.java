package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;

public class StateProfecia extends State {
  
  //private Cartas cartasProfecia = new Cartas(getOnirim(), "");

  public StateProfecia(Onirim onirim) {
    super(onirim);

  }

  @Override
  public void onEnter() {
    Carta carta;
    for (int i = 0; i < 5; i++) {
      carta = getOnirim().getPilhaJogo().tiraUltimaCarta();
      getOnirim().getPilhaCartasProfecia().add(carta);
    }
    getOnirim().nextStateProfeciaDescarta();
  }

//  public Cartas getCartasProfecia() {
//    return cartasProfecia;
//  }
  

//  @Override
//  public void profecia() {
//    super.profecia();
//    getOnirim().nextStateComprar();
//  }
// 
  @Override
  public String toString() {
    return "Profecia";
  }
  
  
}
