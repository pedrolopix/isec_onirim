package BaseEstados;

import Base.Onirim;

public class StateGanhou extends State {
  
  //CONSTRUTOR
  public StateGanhou(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void onEnter() {
    super.onEnter();
    getOnirim().doGanhouJogo();
    //getOnirim().nextStateFinal();    
  }

  @Override
  public void terminarJogo() {
    getOnirim().nextStateFinal();
  }

  
  
  @Override
  public String toString() {
    return "Ganhou";
  }
}
