package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.CartaPorta;
import BaseCartas.CartaSonho;

public class StateComprouSonho extends State {

  //CONSTRUTOR
  public StateComprouSonho(Onirim onirim) {
    super(onirim);
  }

  /*
   * "Descartar da mão uma Carta Chave.", "Enviar uma Carta Porta ja jogada para
   * o Limbo.", "Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.",
   * "Descartar Mão e comprar nova, tal como inicialmente."};
   */
  @Override
  public void comprouSonho(int opcao) throws Exception {
    super.comprouSonho(opcao);
    switch (opcao) {
      case 1:
        executaPesadeloUm();
        break;
      case 2:
        executaPesadeloDois();
        break;
      case 3:
        executaPesadeloTres();
        break;
      case 4:
        executaPesadeloQuatro();
        break;
    }
  }

  //1 - Descartar da mao uma Carta Chave.
  public void executaPesadeloUm() throws Exception {
    Carta carta = getOnirim().getMao().temChave();
    if (carta == null) {
      getOnirim().doNaoExisteChaveNaMao();
      getOnirim().nextStateComprouSonho();
      throw new Exception("Não tem nenhuma chave na mão para poder descartar!");
    }
    getOnirim().getMao().moverCartaPara(carta, getOnirim().getPilhaDescarte());
    getOnirim().nextStateComprar();
  }

  //2 - Enviar uma Carta Porta ja jogada para o Limbo.
  public void executaPesadeloDois() throws Exception {
    Carta carta = getOnirim().getPilhaCartasJogadas().getNextPorta();
    if (carta == null) {
      getOnirim().doNaoExistePortaNoJogo();
      getOnirim().nextStateComprouSonho();
      throw new Exception("Ainda nao foi jogada uma carta Porta para que possa enviar para o Limbo!");
    }
    getOnirim().getPilhaCartasJogadas().moverCartaPara(carta, getOnirim().getPilhaLimbo());
    getOnirim().nextStateComprar();
  }

  //3 - Revelar 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.
  public void executaPesadeloTres() throws Exception {
    int ver = 5;

    if (getOnirim().getPilhaJogo().isEmpty()) {
      getOnirim().nextStatePerdeu();
      throw new Exception("Nao tem mais cartas na pilha de jogo. Perdeu!");
    }

    if (getOnirim().getPilhaJogo().size() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(3);
      getOnirim().nextStateComprouSonho();
      throw new Exception("Nao tem cartas suficientes na pilha de jogo!");
    }   
    
    int ultima = getOnirim().getPilhaJogo().size() - 1;
    for (int i = ultima; i > ultima - ver && i > 0; i--) {
      Carta carta = getOnirim().getPilhaJogo().get(i);
      if ((carta instanceof CartaSonho) || (carta instanceof CartaPorta)) {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaLimbo());
      }
      else {
        getOnirim().getPilhaJogo().moverCartaPara(carta, getOnirim().getPilhaDescarte());
      }
    }
    getOnirim().nextStateComprar();
  }

  //4 - Descartar Mao e comprar nova, tal como inicialmente.
  public void executaPesadeloQuatro() throws Exception {
    if (getOnirim().getPilhaJogo().temLabirintos() < 5) {
      getOnirim().doNaoExistemCartasSuficientes(4);
      getOnirim().nextStateComprouSonho();
      throw new Exception("Nao tem cartas Labirinto suficientes na pilha de jogo!");
    }        
    getOnirim().getPilhaDescarte().addAll(getOnirim().getMao());
    getOnirim().getMao().clear();
    getOnirim().iniciaMao();
    getOnirim().nextStateComprar();
  }

  @Override
  public String toString() {
    return "Comprou Sonho";
  }
}
