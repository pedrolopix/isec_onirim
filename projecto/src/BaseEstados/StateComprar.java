package BaseEstados;

import BaseCartas.Carta;
import Base.Onirim;

public class StateComprar extends State {

  public StateComprar(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void onEnter() {
    super.onEnter();
    if (getOnirim().getMao().size() >= 5) {
      getOnirim().baralhar();
      getOnirim().nextStateFaseJogo();
      return;
    }

    Carta carta = getOnirim().getPilhaJogo().getUltima();
    if (carta == null) {
      getOnirim().nextStatePerdeu();
      return;
    }
    //Passa responsabilidade para a carta
    carta.foiComprada(getOnirim());
  }

  @Override
  public String toString() {
    return "Comprar";
  }
}
