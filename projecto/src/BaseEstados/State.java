package BaseEstados;

import Base.Onirim;
import BaseCartas.Carta;
import java.io.Serializable;

public abstract class State implements Serializable{

  private Onirim onirim;
  

  //CONSTRUTOR
  public State(Onirim onirim) {
    this.onirim = onirim;
  }

  //GET
  public Onirim getOnirim() {
    return onirim;
  }

  

  public void onEnter() {}
  
  public void iniciarJogo(){}
  
  public void sairDoJogo(){}
  
  public void jogarCarta(Carta carta) throws Exception{}
  
  public void descartarCarta(Carta carta){}
  
  public void terminarJogo() {}
  
  public void comprouPortaSemChave() { }

  public void comprouPortaComChave(int opcao) { }
  
  public void descartaCartaProfecia(Carta carta) { }
  
  public void organizarCartasProfecia(Carta carta) { }
    
  public void comprouSonho(int opcao) throws Exception{}
 
  

  

  
}
