package BaseEstados;

import Base.BaralhoBase;
import Base.Onirim;

public class StateIniciarJogo extends State {

  //CONSTRUTOR
  public StateIniciarJogo(Onirim onirim) {
    super(onirim);
  }

  /*
  System.out.println("0. Jogo Base");
  System.out.println("1. Jogo Base + Expansao 1 : Livro dos Passos");
  System.out.println("2. Jogo Base + Expansao 2 : As Torres ");
  System.out.println("3. Jogo Base + Expansao 3 : Premonições Sombrias");
  */

  //PREPARA O JOGO BASE DO ONIRIM
  @Override
  public void onEnter(){
    preparaJogo();
    
    //ADICIONA OS BARALHOS A PILHA DE JOGO
    for (int i = 0; i < getOnirim().getBaralhos().size(); i++) {
      getOnirim().getPilhaJogo().addAll(getOnirim().getBaralhos().get(i).getCartas());
    }
    
    //BARALHA E INICIA MAO
    for(int i=0; i<4; i++){
      getOnirim().getPilhaJogo().baralhar();
    }    
    getOnirim().iniciaMao();
    getOnirim().baralhar();
    
    getOnirim().nextStateFaseJogo();
  }

  protected void preparaJogo() {
    getOnirim().getPilhaJogo().clear();
    getOnirim().getPilhaLimbo().clear();
    getOnirim().getPilhaDescarte().clear();
    getOnirim().getMao().clear();
    getOnirim().getPilhaCartasJogadas().clear();

    //CRIA BARALHO BASE
    getOnirim().getBaralhos().add(new BaralhoBase());    
  }
  

  @Override
  public void sairDoJogo() {
    getOnirim().nextStateFinal();
  }  

  @Override
  public String toString() {
    return "Novo Jogo";
  }    
}
