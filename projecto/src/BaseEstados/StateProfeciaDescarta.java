package BaseEstados;

import BaseCartas.Carta;
import Base.Onirim;

public class StateProfeciaDescarta extends State{

  public StateProfeciaDescarta(Onirim onirim) {
    super(onirim);
  }

  @Override
  public void descartaCartaProfecia(Carta carta) {
    getOnirim().getPilhaCartasProfecia().moverCartaPara(carta, getOnirim().getPilhaDescarte());
    getOnirim().nextStateProfeciaOrganiza();
  }
  
  

  @Override
  public String toString() {    
    return "Profecia Descarta";
  }
  
  
  
  
  
  
  
  
}
