package ExpansaoTorres;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.State;
import ExpansaoTorresCartas.CartaTorre;
import ExpansaoTorresEstados.*;
import java.util.ArrayList;

public class OnirimExp2 extends Onirim{
  private State stateIniciarJogo = new StateIniciarJogoExp2(this);
  private State stateFaseJogo = new StateFaseJogoExp2(this);
  private State stateOrganizarTorres = new StateOrganizarTorres(this);
  private State stateTorreDescartada = new StateTorreDescartada(this);
  private State stateTorreDescartadaOrganiza = new StateTorreDescartadaOrganiza(this);  
  private State stateComprouSonho = new StateComprouSonhoExp2(this);
  private State stateComprouSonhoTorres = new StateComprouSonhoTorres(this);
  private State stateDescartarTorre = new StateDescartarTorreJogada(this);
  private State stateComprouPortaComChave = new StateComprouPortaComChaveExp2(this);
  
  private int maxTorres = 4;
  private Cartas pilhaTorres = new Cartas(this, "Pilha das Torres");
  private Cartas pilhaAuxiliar = new Cartas(this, "Pilha auxiliar");

  //COSNTRUTOR
  public OnirimExp2() {
    super();    
  }

  @Override
  public OnirimExp2 getOnirim() {
    return (OnirimExp2)super.getOnirim();
  }

  //GET   
  @Override public State getStateIniciarJogo() { return stateIniciarJogo; }
  @Override public State getStateFaseJogo() { return stateFaseJogo; }
  public State getStateOrganizarTorres() { return stateOrganizarTorres; }
  public State getStateTorreDescartada() { return stateTorreDescartada; }
  public State getStateTorreDescartadaOrganiza() { return stateTorreDescartadaOrganiza; }
  @Override public State getStateComprouSonho() { return stateComprouSonho; }
  public State getStateComprouSonhoTorres() { return stateComprouSonhoTorres; }
  public State getStateDescartarTorre() { return stateDescartarTorre; }
  @Override public State getStateComprouPortaComChave() { return stateComprouPortaComChave; }

  public int getMaxTorres() { return maxTorres; }
  @Override public Cartas getPilhaAuxiliar() { return pilhaAuxiliar; }
  @Override public Cartas getPilhaTorres() { return pilhaTorres; }

  
  //SET
  @Override
  protected void setNextState(State state) {
    super.setNextState(state);
  }
  @Override public void nextStateIniciarJogo() { setNextState(getStateIniciarJogo()); }
  @Override public void nextStateFaseJogo() { setNextState(getStateFaseJogo()); }
  public void nextStateOrganizarTorres() { setNextState(getStateOrganizarTorres()); }
  public void nextStateTorreDescartada() { setNextState(getStateTorreDescartada()); }
  public void nextStateTorreDescartadaOrganiza() { setNextState(getStateTorreDescartadaOrganiza()); }
  
  @Override public void nextStateComprouSonho() { setNextState(getStateComprouSonho()); }
  public void nextStateComprouSonhoTorres() { setNextState(getStateComprouSonhoTorres()); }
  public void nextStateDescartarTorre() { setNextState(getStateDescartarTorre()); }
  @Override public void nextStateComprouPortaComChave() { setNextState(getStateComprouPortaComChave()); }
  
  
  
  
  //############################################################################
  //##
  //## FUNÇÕES DE COMUNICAÇÃO COM O INTERFACE TEXTO / Modelo
  
//  @Override
//  public void organizarTorres(Carta carta) { //StateOrganizarTorres
//    super.organizarTorres(carta);
//  }
//
//  @Override
//  public void torreDescartadaOrganizaCartas(Carta carta) { //StateTorreDescartada
//    super.torreDescartadaOrganizaCartas(carta);
//  }
//  
//  @Override
//  public void descartarTorreJogada() { //StateComprouSonhoTorres
//    super.descartarTorreJogada();
//  }
//
//  @Override
//  public void colocarPesadeloLimbo() { //StateComprouSonhoTorres
//    super.colocarPesadeloLimbo();
//  }
  
  
  
  //******* EXPANSAO TORRES
  public void organizarTorres(Carta carta, Boolean limbo)  throws Exception { //StateOrganizarTorres
    if (getState() instanceof StateTorres) { 
        ((StateTorres) getState()).organizarTorres(carta, limbo);
    }
  }
  
  public void reOrganizarTorres() {
    if (getState() instanceof StateTorres) { 
        ((StateTorres) getState()).reOrganizarTorres();
    }
  }
  
  public void torreDescartadaOrganizaCartas(Carta carta) { //StateTorreDescartadaOrganiza
    if (getState() instanceof StateTorres) { 
       ((StateTorres) getState()).torreDescartadaOrganizaCartas(carta);
    }
  }
  
  public void comprouSonhoNaExpansaoTorres(int opcao)  throws Exception { //StateComprouSonhoTorres
    if (getState() instanceof StateTorres) {        
    ((StateTorres) getState()).comprouSonhoNaExpansaoTorres(opcao);
    }
  }
    
  public void descartarTorreJogada(Carta carta) { //StateDescartarTorre
    if (getState() instanceof StateTorres) {       
       ((StateTorres) getState()).descartarTorreJogada(carta);
    }
  }
  
  @Override
  public void descartarCarta(Carta carta) { //StateDescartarTorre
    super.descartarCarta(carta);
  }
  
  
  
  
  
  
  
  
  //############################################################################
  
  public boolean jogadaValidaTorres(Carta carta) {
    
    if(getPilhaTorres().isEmpty()){
      return true;
    }
    
    CartaTorre torre = (CartaTorre)(getPilhaTorres().getUltima());
    
    
    ArrayList simboloDireita = torre.getSimboloDir();
    ArrayList simboloEsquerda = ((CartaTorre)carta).getSimboloEsq();
    
    if( simboloDireita.isEmpty() ){
      return true;
    }
    
    if( simboloEsquerda.isEmpty() ){
      return true;
    }
    
    if( simboloIguais(simboloDireita, simboloEsquerda) ){
      return false;
    }
    
    return true;
  }
  
  public boolean simboloIguais(ArrayList simboloDireita, ArrayList simboloEsquerda) {
    ArrayList auxiliar = new ArrayList();
    auxiliar.addAll(simboloDireita);
    auxiliar.addAll(simboloEsquerda);
    int iguais = 0;
    
    for(int i=0; i<auxiliar.size(); i++){
      for(int j=0; j<auxiliar.size(); j++){
        if(auxiliar.get(i) == auxiliar.get(j)){
          iguais ++;
        }          
      }
      if(iguais > 1){
        return true;
      }
      iguais=0;        
    }
    return false;
  }
  
  public boolean torreIgualJaJogada(Carta carta) {
    if(pilhaTorres.isEmpty()){
      return false;
    }
    
    for(int i=0; i<pilhaTorres.size(); i++){
      if(carta.SameColor(pilhaTorres.get(i))){
        return true;
      }
    }    
    return false;
  }
  
  public boolean encontrouTodasTorres(){
    if(getPilhaTorres().size() == getMaxTorres()){
      return true;
    }
    return false;      
  }
  
  
  
  
  
 
  
  @Override
  public String toString() {
    return "Onirim extenção 2 - As Torres";
  }

  

  

  
  
  
  
  
  
  
  
  
  
}
