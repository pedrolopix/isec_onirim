package BaseCartas;

import Base.Onirim;
import java.util.ArrayList;

public class CartaPorta extends CartaCor {

  //CONSTRUTOR
  public CartaPorta(Cores cor) {
    super(cor);
  }
  
  @Override
  public void foiComprada(Onirim onirim) {
    onirim.nextStateComprouPorta();
  }
  
  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.doCartaPortaDescartada(this);
  }

  @Override
  public String toString() {
    return super.toString() + "P";
  }
}
