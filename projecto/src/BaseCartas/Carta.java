package BaseCartas;

import Base.Onirim;
import java.io.Serializable;

public abstract class Carta implements Serializable {
  

  public abstract void foiComprada(Onirim onirim);

  public abstract void foiDescartada(Onirim onirim);
  
  public boolean SameColor(Carta carta) {
    return false;
  }  

  @Override
  public abstract String toString();

  public boolean SameSimbolo(Carta carta) {
    return false;
  }
}
