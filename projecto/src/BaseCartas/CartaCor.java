package BaseCartas;

import Base.Onirim;

public abstract class CartaCor extends Carta {

  private Cores cor;

  //CONSTRUTOR
  public CartaCor(Cores cor) {
    this.cor = cor;
  }


  public Cores getCor() { return cor; }

  @Override
  public abstract void foiComprada(Onirim onirim);

  @Override
  public abstract void foiDescartada(Onirim onirim);

  @Override
  public boolean SameColor(Carta carta) {
    if (carta instanceof  CartaCor) {
      return ((CartaCor)carta).getCor()==getCor();
    }
    return false;
  }
  
  
  
  @Override
  public String toString() {
    return cor.toString().toUpperCase().substring(0, 1);
  }
}
