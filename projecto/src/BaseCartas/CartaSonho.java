package BaseCartas;

import Base.Onirim;

public class CartaSonho extends Carta {
  

  @Override
  public void foiComprada(Onirim onirim) {
    onirim.getPilhaJogo().moverCartaPara(this, onirim.getPilhaDescarte());
    onirim.nextStateComprouSonho();
  }

  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.doCartaSonhoDescartada();
  }
  
 
  @Override
  public String toString() {
    return "S";
  }


  
  
}
