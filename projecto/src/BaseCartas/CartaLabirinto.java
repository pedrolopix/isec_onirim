package BaseCartas;

import Base.Onirim;
import java.util.ArrayList;

public class CartaLabirinto extends CartaCor {

  private Simbolos simbolo;

  //CONSTRUTOR
  public CartaLabirinto(Cores cor, Simbolos simbolo) {
    super(cor);
    this.simbolo = simbolo;
  }

  public ArrayList getSimboloEsq() {
    return null;
  }

  public ArrayList getSimboloDir() {
    return null;
  }

  public Simbolos getSimbolo() {
    return simbolo;
  }

  @Override
  public Cores getCor() {
    return super.getCor();
  }

  @Override
  public void foiComprada(Onirim onirim) {
    onirim.getPilhaJogo().moverCartaPara(this, onirim.getMao());
    onirim.nextStateComprar();
  }

  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.getMao().moverCartaPara(this, onirim.getPilhaDescarte());
    onirim.doCartaLabirintoDescartada(this);
    if (this.getSimbolo().compareTo(Simbolos.chave) == 0) {
      onirim.nextStateProfecia();
      return;
    }
    onirim.nextStateComprar();
  }

  @Override
  public String toString() {
    if (simbolo!=null) {
    return super.toString() + simbolo.toString().toUpperCase().substring(0, 1);
    } else
    {
      return super.toString();
    }
  }

  @Override
  public boolean SameSimbolo(Carta carta) {
    if ((carta instanceof CartaLabirinto) && ((CartaLabirinto) carta).getSimbolo()!=null) {
      return ((CartaLabirinto) carta).getSimbolo() == getSimbolo();
    }
    return false;
  }
  
  public boolean SameSimbolo(Simbolos simbolo) {
    return (getSimbolo()!=null && getSimbolo()==simbolo);
  }
  
}
