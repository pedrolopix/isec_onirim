package ExpansaoTorresCartas;

import Base.Onirim;
import BaseCartas.Carta;
import BaseCartas.CartaLabirinto;
import BaseCartas.Cores;
import BaseCartas.Simbolos;
import ExpansaoTorres.OnirimExp2;
import java.util.ArrayList;

public class CartaTorre extends CartaLabirinto {

  private ArrayList<Simbolos> simbolosEsq = new ArrayList<>();
  private ArrayList<Simbolos> simbolosDir = new ArrayList<>();
  private int valor;

  //CONSTRUTOR
  public CartaTorre(Cores cor, ArrayList sEsq, ArrayList sDir, int valor) {
    super(cor,null);
    if (sEsq!=null )this.simbolosEsq.addAll(sEsq);
    if (sDir!=null ) this.simbolosDir.addAll(sDir);
    this.valor = valor;
  }
 
  @Override
  public ArrayList getSimboloEsq() {
    return this.simbolosEsq;
  }

  @Override
  public ArrayList getSimboloDir() {
    return this.simbolosDir;
  }

  public int getValor() {
    return this.valor;
  }

  @Override
  public Cores getCor() {
    return super.getCor();
  }




  @Override
  public void foiDescartada(Onirim onirim) {  
    onirim.getMao().moverCartaPara(this, onirim.getPilhaDescarte());    
    ((OnirimExp2)onirim).nextStateTorreDescartada();
  }

  
  
  
  @Override
  public String toString() {
    return super.toString() + "T";
  }
}
