package ExpansaoTorresCartas;

import ExpansaoTorresCartas.CartaTorre;
import BaseCartas.Cores;
import BaseCartas.Simbolos;
import Base.Baralho;
import java.util.ArrayList;

public class BaralhoTorres extends Baralho{

  //CONSTRUTOR
  public BaralhoTorres() {
    super();
  }
  
  @Override
  public void CriarCartas() {
    
    ArrayList<Simbolos> luaSol = new ArrayList<>();
    ArrayList<Simbolos> lua = new ArrayList<>();
    ArrayList<Simbolos> sol = new ArrayList<>();
    
    luaSol.add(Simbolos.lua);
    luaSol.add(Simbolos.sol);
    lua.add(Simbolos.lua);
    sol.add(Simbolos.sol);
    
    //TORRES AZUIS
    getCartas().add( new CartaTorre(Cores.azul, lua, luaSol, 3) );
    getCartas().add( new CartaTorre(Cores.azul, lua, lua, 4) );
    getCartas().add( new CartaTorre(Cores.azul, lua, null, 5) );
    
    //TORRES CASTANHAS
    getCartas().add( new CartaTorre(Cores.castanho, luaSol, sol, 3) );
    getCartas().add( new CartaTorre(Cores.castanho, lua, sol, 4) );
    getCartas().add( new CartaTorre(Cores.castanho, null, sol, 5) );
    
    //TORRES ENCARNADAS
    getCartas().add( new CartaTorre(Cores.encarnado, luaSol, lua, 3) );
    getCartas().add( new CartaTorre(Cores.encarnado, sol, lua, 4) );
    getCartas().add( new CartaTorre(Cores.encarnado, null, lua, 5) );
    
    //TORRES VERDES
    getCartas().add( new CartaTorre(Cores.verde, sol, luaSol, 3) );
    getCartas().add( new CartaTorre(Cores.verde, sol, sol, 4) );
    getCartas().add( new CartaTorre(Cores.verde, sol, null, 5) );    
  }
  
}
