/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceGraf;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;

/**
 *
 * @author pedro
 */
public class PTButton extends JButton {

  public PTButton(String text) {
    super(text);
    setMinimumSize(new Dimension(150, 100));
    setMaximumSize(new Dimension(150, 100));
    setPreferredSize(new Dimension(150, 100));
    setBackground(Color.WHITE);
  }
}
