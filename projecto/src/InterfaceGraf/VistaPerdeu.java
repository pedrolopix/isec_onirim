/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceGraf;

import Onirim.Modelo;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.JLabel;

/**
 *
 * @author pedro
 */
class VistaPerdeu  extends VistaPanel{
  
  public VistaPerdeu(final Modelo modelo) {
    super(modelo,"Perdeu");
    JLabel lbl = new JLabel("Perdeu!!!");
    lbl.setFont(new Font("",Font.BOLD,24));
    Box box = Box.createHorizontalBox();
    box.add(Box.createVerticalGlue());
    box.add(Box.createHorizontalGlue());
    box.add(lbl);
    box.add(Box.createHorizontalGlue());
    box.add(Box.createVerticalGlue());
    panel.setLayout(new GridLayout());
    panel.add(box);
    
  }
}