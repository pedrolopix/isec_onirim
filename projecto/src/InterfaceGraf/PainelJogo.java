package InterfaceGraf;

import ExpansaoTorres.OnirimExp2;
import Onirim.Modelo;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;

public class PainelJogo extends JPanel implements Observer {

  Modelo modelo;
  JPanel painelNorte;
  JPanel painelEste;
  JPanel painelOeste;
  JPanel painelSul;
  JPanel painelCentro;
  CardLayout cl;
  CardLayout clCentro;
  VistaCartasMao vistaCartasMao;
  VistaPilhas vistaPilhas;
  VistaCartasJogadas vistaCartasJogadas;
  VistaCartasJogadasPortas vistaCartasJogadasPortas;
  VistaComprouPortaComChave vistaComprouPortaComChave;
  VistaComprouPortaSemChave vistaComprouPortaSemChave;
  VistaProfeciaDescarta vistaProfeciaDescarta;
  VistaProfeciaOrganiza vistaProfeciaOrganiza;
  VistaComprouSonho vistaComprouSonho;
  VistaPerdeu vistaPerdeu;
  VistaGanhou vistaGanhou;
  VistaCartasTorres vistaCartasTorres;
  VistaOrganizarTorres vistaOrganizarTorres;
  VistaTorreDescartadaOrganiza vistaTorreDescartadaOrganiza;
  VistaComprouSonhoTorres vistaComprouSonhoTorres;
  VistaDescartarTorreJogada vistaDescartarTorre;

  //CONSTRUTOR
  public PainelJogo(Modelo modelo) {
    this.modelo = modelo;
    modelo.addObserver(this);
    setLayout(new BorderLayout());

    painelNorte = new JPanel();
    painelEste = new JPanel();
    painelOeste = new JPanel();
    painelSul = new JPanel();

    //VISTAS BASE
    vistaPilhas = new VistaPilhas(modelo);
    vistaCartasMao = new VistaCartasMao(modelo);
    vistaCartasJogadas = new VistaCartasJogadas(modelo);
    vistaCartasJogadasPortas = new VistaCartasJogadasPortas(modelo);
    vistaComprouPortaComChave = new VistaComprouPortaComChave(modelo);
    vistaComprouPortaSemChave = new VistaComprouPortaSemChave(modelo);
    vistaProfeciaDescarta = new VistaProfeciaDescarta(modelo);
    vistaProfeciaOrganiza = new VistaProfeciaOrganiza(modelo);
    vistaComprouSonho = new VistaComprouSonho(modelo);
    vistaPerdeu = new VistaPerdeu(modelo);
    vistaGanhou = new VistaGanhou(modelo);
    //VISTAS - TORRES
    vistaCartasTorres = new VistaCartasTorres(modelo);
    vistaOrganizarTorres = new VistaOrganizarTorres(modelo);
    vistaTorreDescartadaOrganiza = new VistaTorreDescartadaOrganiza(modelo);
    vistaComprouSonhoTorres = new VistaComprouSonhoTorres(modelo);
    vistaDescartarTorre = new VistaDescartarTorreJogada(modelo);



    add(painelNorte, BorderLayout.NORTH);
    painelNorte.setLayout(new GridLayout(1, 0));
    painelNorte.setBackground(Color.WHITE);
    painelNorte.add(vistaCartasTorres);//ORGINAL    
    //painelNorte.add(new VistaPilhaDescarte(modelo, "Pilha Descarte") );//PARA APAGAR

    clCentro= new  CardLayout();
    painelCentro= new JPanel();
    painelCentro.setLayout(clCentro);
    add(painelCentro,BorderLayout.CENTER); 
    
    JPanel j= new JPanel();
    j.setLayout(new GridLayout(2,0));
    j.add(vistaCartasJogadasPortas);
    j.add(vistaCartasJogadas);
    painelCentro.add(j,"Fase Jogo"); 
    
    //add(painelEste, BorderLayout.EAST);
    
    //PAINEL OESTE - PILHAS DE JOGO
    painelOeste.add(vistaPilhas);
    painelOeste.setBackground(CartaUtils.GrayWhite());
    add(painelOeste, BorderLayout.WEST);

    add(painelSul, BorderLayout.SOUTH);
    cl = new CardLayout();
    painelSul.setLayout(cl);
    painelSul.add(vistaCartasMao, "Fase Jogo");
    painelCentro.add(vistaComprouPortaComChave, "Comprou Porta Com Chave");
    painelCentro.add(vistaComprouPortaSemChave, "Comprou Porta Sem Chave");
    painelCentro.add(vistaProfeciaDescarta, "Profecia Descarta");
    painelCentro.add(vistaProfeciaOrganiza, "Profecia Organiza");
    painelCentro.add(vistaComprouSonho, "Comprou Sonho");
    painelCentro.add(vistaPerdeu, "Perdeu");
    painelCentro.add(vistaGanhou, "Ganhou");

    painelCentro.add(vistaOrganizarTorres, "Organizar Torres");
    painelCentro.add(vistaTorreDescartadaOrganiza, "Torre Descartada Organiza");
    painelCentro.add(vistaComprouSonhoTorres, "Comprou Sonho Torres");
    painelCentro.add(vistaDescartarTorre, "Descartar Torre");

   

    update(modelo, null);
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      return;
    }

    if (modelo.getOnirim() instanceof OnirimExp2) {
      painelNorte.setVisible(true);
    }
    else {
      painelNorte.setVisible(false);
    }
        
    clCentro.show(painelCentro, modelo.getState().toString());
  }
}
