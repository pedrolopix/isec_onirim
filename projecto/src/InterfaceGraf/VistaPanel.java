package InterfaceGraf;

import Onirim.Modelo;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class VistaPanel extends JPanel {

  Modelo modelo;
  JPanel panel;

  public VistaPanel(Modelo modelo, String label) {

    this.modelo = modelo;
    
    setBackground(Color.WHITE);
    //setLayout(new BorderLayout(10, 10));
    setLayout(new BorderLayout());
    

    //PAINEL NORTE
    JPanel p = new JPanel();
    p.setBackground(CartaUtils.GrayWhite());

    JLabel lbl = new JLabel(label);
    p.add(lbl);

    add(p, BorderLayout.NORTH);

    //PAINEL CENTRAL    
    panel = new JPanel();
    panel.setBackground(Color.WHITE);
    add(panel);

        
    //PAINEL SUL
    //JPanel p2 = new JPanel();
    //p2.setBackground(Color.WHITE);
    //add(p2, BorderLayout.SOUTH);

    
    setVisible(true);
  }
}