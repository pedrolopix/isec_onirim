package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.StateProfeciaOrganiza;
import Onirim.Modelo;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

public class VistaProfeciaOrganiza extends VistaCartas implements Observer {

  public VistaProfeciaOrganiza(Modelo modelo) {
    super(modelo, "Profecia! Clic numa carta colocar no topo do jogo.");
  }

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaCartasProfecia();
  }

  @Override
  protected int getNCartas() {
    return modelo.getPilhaCartasProfecia().size();
  }

  @Override
  protected boolean clicable() {
    return true;
  }

  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta) {
    if (e.getButton() == MouseEvent.BUTTON1) {
      modelo.organizarCartasProfecia(carta);
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null || modelo.getState() instanceof StateProfeciaOrganiza) {
      super.update(o, arg);
    }
  }
}
