package InterfaceGraf;

import BaseCartas.Cartas;
import ExpansaoTorres.OnirimExp2;
import Onirim.Modelo;

class VistaCartasTorres extends VistaCartas {

  public VistaCartasTorres(Modelo modelo) {
    super(modelo, "Torres");
  }

  @Override
  protected Cartas getCartas() {
    if (modelo.getOnirim() instanceof OnirimExp2) {
      return ((OnirimExp2) modelo.getOnirim()).getPilhaTorres();
    }
    return null;
  }

  @Override
  protected int getNCartas() {
    if (modelo.getOnirim() instanceof OnirimExp2) {
      return ((OnirimExp2) modelo.getOnirim()).getPilhaTorres().size();
    }
    return 0;
  }

  @Override
  protected boolean clicable() {
    return false;
  }
}
