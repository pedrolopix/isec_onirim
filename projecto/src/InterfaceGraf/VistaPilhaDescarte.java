package InterfaceGraf;

import BaseCartas.Cartas;
import Onirim.Modelo;
import java.util.Observer;

public class VistaPilhaDescarte extends VistaCartas implements Observer{

  public VistaPilhaDescarte(Modelo modelo, String label) {
    super(modelo, label);
  }
  
  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaDescarte();
  }

  @Override
  protected int getNCartas() {
    return modelo.getPilhaDescarte().size();
  }

  @Override
  protected boolean clicable() {
    return false;
  }
  
}
