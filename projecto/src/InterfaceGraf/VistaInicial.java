package InterfaceGraf;

import Onirim.Modelo;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VistaInicial extends JPanel {

  Modelo modelo;

  public VistaInicial(Modelo modelo) {
    this.modelo = modelo;
    setLayout( new BorderLayout() );

    
    JPanel panelBotoesCentro = new JPanel();

    panelBotoesCentro.setLayout(new GridLayout(1, 3));
    panelBotoesCentro.setBackground(Color.WHITE);
    //BOTAO JOGO BASE
    JButton button1 = new PTButton("Onirim Base");
    button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    button1.addActionListener(new JogoBase());
    button1.setMinimumSize(new Dimension(150, 150));
    button1.setMaximumSize(new Dimension(150, 150));
    button1.setPreferredSize(new Dimension(150, 150));

    Box box1 = Box.createHorizontalBox();
    box1.add(Box.createVerticalGlue());
    box1.add(Box.createHorizontalGlue());
    box1.add(button1);
    box1.add(Box.createHorizontalGlue());
    box1.add(Box.createVerticalGlue());
    panelBotoesCentro.add(box1);


    //IMAGEM CENTRAL ONIRIMM
    JLabel lblOnirim = new JLabel();
    lblOnirim.setIcon(CartaUtils.createImageIcon("Imagens/Cartas/ONIRIM.gif"));

    Box box2 = Box.createHorizontalBox();
    box2.add(Box.createVerticalGlue());
    box2.add(Box.createHorizontalGlue());
    box2.add(lblOnirim);
    box2.add(Box.createHorizontalGlue());
    box2.add(Box.createVerticalGlue());
    panelBotoesCentro.add(box2);


    //BOTAO JOGAO + EXPANSAO
    JButton button2 = new PTButton("Onirim + As Torres");
    button2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    button2.addActionListener(new JogoTorres());
    button2.setMinimumSize(new Dimension(150, 150));
    button2.setMaximumSize(new Dimension(150, 150));
    button2.setPreferredSize(new Dimension(150, 150));

    Box box3 = Box.createHorizontalBox();
    box3.add(Box.createVerticalGlue());
    box3.add(Box.createHorizontalGlue());
    box3.add(button2);
    box3.add(Box.createHorizontalGlue());
    box3.add(Box.createVerticalGlue());
    panelBotoesCentro.add(box3);
 
        
    //PAINEL CENTRO - botoes e imagem
    add(panelBotoesCentro, BorderLayout.CENTER);

    
    
    JPanel panelBotoesSul = new JPanel();
    panelBotoesSul.setBackground(CartaUtils.GrayWhite());
    JLabel l= new JLabel("<html>Desenvolvido por:<ul><li> Tiago Rosado (20105059)</li><li> Pedro Lopes (21200565)</li></ul><br/>Programação Avançada, ISEC 2011/2012</html>");
    panelBotoesSul.add(l);
    add(panelBotoesSul, BorderLayout.SOUTH);
    
  }
  
  
  

  private class JogoBase implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      modelo.novoJogoBase();
    }
  }

  private class JogoTorres implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      modelo.novoJogoExp2();
    }
  }
}
