package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import Onirim.Modelo;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

abstract public class VistaCartas extends VistaPanel implements Observer {

  private GridBagLayout gl;
  ArrayList<JLabel> lblCartas = new ArrayList<>();

  //CONSTRUTOR
  public VistaCartas(Modelo modelo, String label) {
    super(modelo, label);
    modelo.addObserver(this);
    gl = new GridBagLayout();

    panel.setLayout(gl);
    panel.setMinimumSize(new Dimension(0, 140));
    panel.setMaximumSize(new Dimension(0, 140));
    panel.setPreferredSize(new Dimension(0, 140));
    setVisible(true);
  }

  abstract protected Cartas getCartas();

  abstract protected int getNCartas();

  abstract protected boolean clicable();

  protected void OnCartaClick(MouseEvent e, Carta carta) {
  }

  private void clearCards() {
    JLabel labelImagem;
    for (int i = 0; i < lblCartas.size(); i++) {
      labelImagem = lblCartas.get(i);
      labelImagem.setIcon(null);
    }
  }

  private void buildCards() {
    JLabel labelImagem;
    ImageIcon imgIcon;
    int p = 0;
    int i;
    Cartas c = getCartas();

    if (c == null) {
      return;
    }
    int nCartas = getNCartas();

    if (nCartas < lblCartas.size()) {
      for (int j = 0; j < lblCartas.size(); j++) {
        labelImagem = lblCartas.get(j);
        labelImagem.setIcon(null);
        labelImagem.setVisible(false);
        //remove(labelImagem);
      }
    }
    
    //calcular inicial
    int ini=c.size();
    i=c.size();
    for(int j=c.size()-1;j>=0 && p<getNCartas();j--){
      if (isFiltroOK(c.get(j))) {
        ini=j;
        p++;
      }
    }

    //int ini = c.size() - nCartas;
//    if (ini < 0) {
//      ini = 0;
//      gl.setColumns(c.size());
//    } else {
//      gl.setColumns(nCartas);
//    }

    i = ini;
    p=0;
    while (i >= 0 && i < c.size() && p<getNCartas()) {
      //for (int i = ini; i >= 0 && i < c.size(); i++) {
      if (isFiltroOK(c.get(i))) {

        imgIcon = CartaUtils.createImageIcon(c.get(i));
        if (lblCartas.size() <= p) {
          //criar objecto para a carta
          labelImagem = new JLabel(imgIcon);
          labelImagem.setBorder(new LineBorder(Color.WHITE, 5));

          labelImagem.addMouseListener(new ClicLabelCarta(i));


          Box box = Box.createHorizontalBox();
          box.add(Box.createVerticalGlue());
          box.add(Box.createHorizontalGlue());
          box.add(labelImagem);
          box.add(Box.createHorizontalGlue());
          box.add(Box.createVerticalGlue());

          panel.add(box);
          lblCartas.add(labelImagem);
        } else {
          labelImagem = lblCartas.get(p);
          labelImagem.setIcon(imgIcon);
          labelImagem.repaint();
          labelImagem.setVisible(true);
        }
        labelImagem.setToolTipText(c.get(i).toString());
        p++;
      }
      i++;
    }
    
    
    for (int j = p; j < lblCartas.size(); j++) {
        labelImagem = lblCartas.get(j);
        labelImagem.setIcon(null);
        labelImagem.setVisible(false);
        //remove(labelImagem);
      }
    
  }


  
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      clearCards();
      return;
    }

    buildCards();
    invalidate();
    validate();
    updateUI();
  }

  protected boolean isFiltroOK(Carta carta) {
    return true;
  }

  //LISTENER ASSOCIADO A CADA LABEL (carta)
  private class ClicLabelCarta implements MouseListener {

    int opcao;

    public ClicLabelCarta(int i) {
      this.opcao = i;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      if (!clicable()) {
        return;
      }
      Carta carta = getCartas().get(opcao);
      OnCartaClick(e, carta);
      JLabel lbl = (JLabel) (e.getSource());
      lbl.setBorder(new LineBorder(Color.WHITE, 5));
      lbl.setCursor(Cursor.getDefaultCursor());
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
      if (!clicable()) {
        return;
      }
      JLabel lbl = (JLabel) (e.getSource());
      lbl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      lbl.setBorder(new LineBorder(Color.ORANGE, 5));
    }

    @Override
    public void mouseExited(MouseEvent e) {
      if (!clicable()) {
        return;
      }
      JLabel lbl = (JLabel) (e.getSource());
      lbl.setCursor(Cursor.getDefaultCursor());
      lbl.setBorder(new LineBorder(Color.WHITE, 5));
    }
  }
}
