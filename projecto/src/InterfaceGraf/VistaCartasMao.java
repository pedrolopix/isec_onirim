package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.StateFaseJogo;
import ExpansaoTorresEstados.StateFaseJogoExp2;
import Onirim.Modelo;
import java.awt.event.MouseEvent;
import java.util.Observable;
import javax.swing.JOptionPane;

public class VistaCartasMao extends VistaCartas {

  public VistaCartasMao(Modelo modelo) {
    super(modelo, "Mão");
  }

  @Override
  protected Cartas getCartas() {
    return modelo.getMao();
  }

  @Override
  protected int getNCartas() {
    return modelo.getMao().size();
  }

  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta) {
    if (e.getButton() == MouseEvent.BUTTON1) {
      try {
        modelo.jogarCarta(carta);
      } catch (Exception ex) {
        JOptionPane.showMessageDialog(panel, ex.getMessage(), "Jogar Labirinto " + carta.toString(), JOptionPane.INFORMATION_MESSAGE);
      }
    }
    if (e.getButton() == MouseEvent.BUTTON3) {
      modelo.descartarCarta(carta);
    }
  }

  @Override
  protected boolean clicable() {
    if (modelo.getOnirim()!=null) {
       return (modelo.getState() instanceof StateFaseJogo);
    } else {
      return false;
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    super.update(o, arg);
  }
  
  
  
}
