package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.StateProfeciaDescarta;
import ExpansaoTorresEstados.StateDescartarTorreJogada;
import Onirim.Modelo;
import java.awt.event.MouseEvent;
import java.util.Observable;


class VistaDescartarTorreJogada  extends VistaCartas{
  
  public VistaDescartarTorreJogada(Modelo modelo) {
    super(modelo, "Das Cartas Torre ja jogadas, clica numa para descartar");
  }

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaTorres();
  }

  @Override
  protected int getNCartas() {
    return modelo.getPilhaTorres().size();
  }

  @Override
  protected boolean clicable() {
    return true;
  }
  
  
  
  
  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta) {
    if (e.getButton()== MouseEvent.BUTTON1) {
       modelo.descartarTorreJogada(carta);
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim()==null || modelo.getState() instanceof StateDescartarTorreJogada) {
       super.update(o, arg);
    }
  }
  
}