package InterfaceGraf;

import BaseCartas.Carta;
import BaseEstados.StateComprouPortaComChave;
import Onirim.Modelo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VistaComprouPortaComChave extends VistaPanel implements Observer {

  JLabel lblCarta;

  public VistaComprouPortaComChave(final Modelo modelo) {
    super(modelo, "Comprou Porta");
    modelo.addObserver(this);

    panel.setLayout(new GridLayout(2, 1));

    //CARTA PORTA LINHA 0 COLUNA 0
    lblCarta = new JLabel();
    Box box1 = Box.createHorizontalBox();
    box1.add(Box.createVerticalGlue());
    box1.add(Box.createHorizontalGlue());
    box1.add(lblCarta);
    box1.add(Box.createHorizontalGlue());
    box1.add(Box.createVerticalGlue()); 
    panel.add(box1);


    //PAINEL DE BOTOES OPCAO LINHA 0 COLUNA 1
    JPanel panelBotoes = new JPanel();
    panelBotoes.setBackground(Color.WHITE);
    panel.add(panelBotoes);

    String[] opcoes = {"Descartar chave da mão e colocar porta em jogo.",
                       "Colocar porta comprada no Limbo."};
    for (int i = 0; i < opcoes.length; i++) {
      JButton button = new PTButton("<html>"+opcoes[i]+"</html>");
      button.addActionListener(new ButtonClic(i+1));

      Box box2 = Box.createVerticalBox();
      box2.add(Box.createVerticalGlue());
      box2.add(Box.createHorizontalGlue());
      box2.add(button);
      box2.add(Box.createHorizontalGlue());
      box2.add(Box.createVerticalGlue());
      panelBotoes.add(box2);
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      return;
    }

    if (modelo.getState() instanceof StateComprouPortaComChave) {
      Carta carta = modelo.getPilhaJogo().getUltima();
      lblCarta.setIcon(CartaUtils.createImageIcon(carta));
    }
  }

  private class ButtonClic implements ActionListener {

    int opcao;

    public ButtonClic(int i) {
      this.opcao = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      modelo.comprouPortaComChave(opcao);
    }
  }
}
