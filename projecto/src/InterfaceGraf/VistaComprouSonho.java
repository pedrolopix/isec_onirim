package InterfaceGraf;

import BaseCartas.Carta;
import BaseEstados.StateComprouSonho;
import Onirim.Modelo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

class VistaComprouSonho extends VistaPanel implements Observer {

  JLabel lblcarta;

  public VistaComprouSonho(Modelo modelo) {
    super(modelo, "Comprou Sonho");
    modelo.addObserver(this);
    
    panel.setLayout(new GridLayout(2, 2));

    //CARTA SONHO LINHA 0 COLUNA 0
    lblcarta = new JLabel();
    Box box1 = Box.createHorizontalBox();
    box1.add(Box.createVerticalGlue());
    box1.add(Box.createHorizontalGlue());
    box1.add(lblcarta);
    //box1.add(lblcarta,BorderLayout.NORTH);
    box1.add(Box.createHorizontalGlue());
    box1.add(Box.createVerticalGlue());
    panel.add(box1);

    //PAINEL DE BOTOES OPCAO LINHA 0 COLUNA 1
    JPanel panelBotoes = new JPanel();
    panelBotoes.setBackground(Color.WHITE);
    panel.add(panelBotoes);
    
    String[] opcoes = {"Descartar da mão uma Carta Chave.",
                       "Enviar uma Carta Porta já jogada para o Limbo.",
                       "Proximas 5 cartas: Sonhos e Portas, Limbo, restantes Descarte.",
                       "Descartar Mão e comprar nova, tal como inicialmente."};
    for (int i = 0; i < opcoes.length; i++) {
      JButton button = new PTButton("<html>"+opcoes[i]+"</html>");

      button.addActionListener(new ButtonClic(i+1));
      Box box = Box.createVerticalBox();
      box.add(Box.createVerticalGlue());
      box.add(Box.createHorizontalGlue());
      box.add(button);
      box.add(Box.createHorizontalGlue());
      box.add(Box.createVerticalGlue());
      panelBotoes.add(box);
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      return;
    }

    if (modelo.getState() instanceof StateComprouSonho) {
      Carta carta = modelo.getPilhaDescarte().getUltima();
      lblcarta.setIcon(CartaUtils.createImageIcon(carta));
    }
  }

  private class ButtonClic implements ActionListener {

    int opcao;

    ButtonClic(int opcao) {
      this.opcao = opcao;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      //JOptionPane.showMessageDialog(panel, opcao, "Comprou Sonho", JOptionPane.INFORMATION_MESSAGE);
      try {
       modelo.comprouSonho(opcao); 
      } catch (Exception ee) {
        JOptionPane.showMessageDialog(panel, ee.getMessage(), "Comprou Sonho", JOptionPane.INFORMATION_MESSAGE);
      }      
    }
  }
}
