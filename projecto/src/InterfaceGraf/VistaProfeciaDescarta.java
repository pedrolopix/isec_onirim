package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import BaseEstados.StateProfeciaDescarta;
import Onirim.Modelo;
import java.awt.event.MouseEvent;
import java.util.Observable;
import javax.swing.JLabel;

public class VistaProfeciaDescarta extends VistaCartas{
  JLabel lblcarta;
  
  public VistaProfeciaDescarta(Modelo modelo) {
    super(modelo, "Profecia! Clic numa carta descartar.");    
  }

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaCartasProfecia();
  }

  @Override
  protected int getNCartas() {
    return modelo.getPilhaCartasProfecia().size();
  }

  @Override
  protected boolean clicable() {
    return true;
  }

  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta) {
    if (e.getButton()== MouseEvent.BUTTON1) {
       modelo.descartarCartaProfecia(carta);       
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim()==null || modelo.getState() instanceof StateProfeciaDescarta) {
       super.update(o, arg);
    }
  }
  
}
