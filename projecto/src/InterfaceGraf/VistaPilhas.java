package InterfaceGraf;


import Onirim.Modelo;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class VistaPilhas extends JPanel implements Observer {
  Modelo modelo;
  JLabel labelPilhaJogo;
  JLabel labelPilhaDescarte;
  Dialogo dlg=null;
  int quantidadePJ;
  int quantidadePD;
  
  private class clicPilhaJogo implements MouseListener {

    public clicPilhaJogo() {
      
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      //modelo.setTeste();
    }

    @Override
    public void mousePressed(MouseEvent e) {
     
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      
    }

    @Override
    public void mouseEntered(MouseEvent e) {
     
    }

    @Override
    public void mouseExited(MouseEvent e) {
     
    }
  }


  
  JPanel vistaPilhaDescarte;

  //CONSTRUTOR
  public VistaPilhas(Modelo modelo) {
    this.modelo = modelo;
    modelo.addObserver(this);

    Box box = Box.createVerticalBox();
    add(box);
    setLayout(new GridLayout(1, 0));
    setBackground(CartaUtils.GrayWhite());

    vistaPilhaDescarte = new VistaPilhaDescarte(modelo, "Pilha Descarte");

    labelPilhaJogo = new JLabel();
    box.add(labelPilhaJogo);
    ImageIcon imgIcon = CartaUtils.createImageIcon("Imagens/Cartas/BACK.gif");
    JLabel labelPJ = new JLabel(imgIcon);
     labelPJ.addMouseListener(new clicPilhaJogo());
    box.add(labelPJ);

    box.add(Box.createVerticalStrut(50));

    labelPilhaDescarte = new JLabel();
    box.add(labelPilhaDescarte);
    ImageIcon imgIcon2 = CartaUtils.createImageIcon("Imagens/Cartas/BACK.gif");
    JLabel labelPD = new JLabel(imgIcon2);
    labelPD.addMouseListener(new clicPilhaDescarte());
    box.add(labelPD);


    update(modelo, null);
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      return;
    }

    quantidadePJ = modelo.getPilhaJogo().size();
    quantidadePD = modelo.getPilhaDescarte().size();

    labelPilhaJogo.setText("Baralho: " + quantidadePJ);
    labelPilhaDescarte.setText("Descarte: " + quantidadePD);


  }

  private class clicPilhaDescarte implements MouseListener {

    public clicPilhaDescarte() {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      //JOptionPane.showMessageDialog(null, "X", "Y", JOptionPane.INFORMATION_MESSAGE);
      
      
      //JOptionPane.showMessageDialog(null, vistaPilhaDescarte, "Pilha Descarte", JOptionPane.PLAIN_MESSAGE);
      
      /*
      JDialog dialogBox = new JDialog();
      JPanel vistaPilhaDescarte = new VistaPilhaDescarte(modelo, "Pilha Descarte");
      dialogBox.setLayout(new BorderLayout());
      dialogBox.setBackground(Color.yellow);
      dialogBox.add(vistaPilhaDescarte, BorderLayout.CENTER);
      dialogBox.setVisible(true);
      */
      if (dlg==null) { dlg=new Dialogo(null);} 
      dlg.setVisible(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
  }

  class Dialogo extends JDialog {

    private Container cp;

    Dialogo(VistaPilhas e) {

      cp = getContentPane();

      
      cp.setLayout(new BorderLayout());
      setBackground(Color.yellow);
      
      JScrollPane sp= new JScrollPane(vistaPilhaDescarte);   
      cp.add(sp);
      // add(vistaPilhaDescarte,BorderLayout.CENTER);
      setLocation(20, 20); // define a localizacao deste componente (frame)
      setSize(1024, 250); // define as dimensoes
      setVisible(false); // torna visivel
      setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//termina a aplicacao
      validate();// dispoe de novo os seus subcomponentes    

    }
  }
  
  
  
  
  
  
}
