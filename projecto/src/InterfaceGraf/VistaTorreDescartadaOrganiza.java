package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import ExpansaoTorresCartas.CartaTorre;
import ExpansaoTorresEstados.StateTorreDescartadaOrganiza;
import Onirim.Modelo;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

class VistaTorreDescartadaOrganiza extends VistaCartas implements Observer {
  
  public VistaTorreDescartadaOrganiza(Modelo modelo) {
    super(modelo, "Profecia Torres! Clica numa carta para colocar de volta na pilha de jogo.");
  }

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaAuxiliar();
  }

  @Override
  protected int getNCartas() {
    CartaTorre cartaTorre = (CartaTorre) modelo.getPilhaDescarte().getUltima();
    return cartaTorre.getValor();
  }

  @Override
  protected boolean clicable() {
    return true;
  }
  
  
  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta) {
    if (e.getButton() == MouseEvent.BUTTON1) {
      modelo.torreDescartadaOrganizaCartas(carta);
    }
  }
  
  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null || modelo.getState() instanceof StateTorreDescartadaOrganiza) {
      super.update(o, arg);
    }
  }
  
}