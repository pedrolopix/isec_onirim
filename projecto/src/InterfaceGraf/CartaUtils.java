package InterfaceGraf;

import BaseCartas.*;
import ExpansaoTorresCartas.CartaTorre;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class CartaUtils {

  static public String getCartaFilename(Carta carta) {
    if (carta == null) {
      return "";
    }

    String s = carta.toString();
    String cor = "";
    String simbolo = "";

    if (carta instanceof CartaCor) {
      switch (((CartaCor) carta).getCor()) {
        case encarnado:
          cor = "RED";
          break;
        case azul:
          cor = "BLUE";
          break;
        case verde:
          cor = "GREEN";
          break;
        case castanho:
          cor = "BROWN";
          break;
      }
    }

    if (carta instanceof CartaSonho) {
      return "Imagens/Cartas/NIGHTMARE.gif";
    }

    if (carta instanceof CartaTorre) {
      String l = "L", r = "R";
      CartaTorre ct = (CartaTorre) carta;
      for (int i = 0; i < ct.getSimboloEsq().size(); i++) {
        l += ct.getSimboloEsq().get(i).toString().toUpperCase().substring(0, 1);
      }
      for (int i = 0; i < ct.getSimboloDir().size(); i++) {
        r += ct.getSimboloDir().get(i).toString().toUpperCase().substring(0, 1);
      }
      simbolo = "TOWER-" + l + "-" + r;
    }

    if ((carta instanceof CartaLabirinto) && ((CartaLabirinto) carta).getSimbolo() != null) {
      switch (((CartaLabirinto) carta).getSimbolo()) {
        case sol:
          simbolo = "SUN";
          break;
        case lua:
          simbolo = "MOON";
          break;
        case chave:
          simbolo = "KEY";
          break;
      }
    }

    if (carta instanceof CartaPorta) {
      simbolo = "DOOR";
    }

    s = "Imagens/Cartas/" + cor + "-" + simbolo + ".gif";
    return s;
  }

  static public ImageIcon createImageIcon(String path) {
    ClassLoader cl = ClassLoader.getSystemClassLoader();
    java.net.URL imgURL = cl.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL);
    }
    else {
      System.err.println(" Nao encontrou o ficheiro: " + path);
      return null;
    }
  }

  static public ImageIcon createImageIcon(Carta carta) {
    return createImageIcon(getCartaFilename(carta));
  }
  
  
  static Color GrayWhite() {
          return new Color(248, 248, 248);
  }
  
  static void configButton(JButton b) {
    
  }
  
}
