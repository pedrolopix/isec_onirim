package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.CartaPorta;
import BaseCartas.Cartas;
import Onirim.Modelo;



public class VistaCartasJogadasPortas extends VistaCartas{

  
  public VistaCartasJogadasPortas(Modelo modelo) {
    super(modelo,"Portas Jogadas");
  }

  @Override
  protected boolean isFiltroOK(Carta carta) {
    return (carta instanceof CartaPorta);
  }
  
  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaCartasJogadas();
  }

  @Override
  protected int getNCartas() {
    return 8;
    //return modelo.getPilhaCartasJogadas().size();
  }

  @Override
  protected boolean clicable() {
    return false;
  }
}

