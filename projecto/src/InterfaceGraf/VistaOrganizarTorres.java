
package InterfaceGraf;

import BaseCartas.Carta;
import BaseCartas.Cartas;
import ExpansaoTorresEstados.StateOrganizarTorres;
import Onirim.Modelo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;


class VistaOrganizarTorres  extends VistaCartas implements Observer{
 
  public VistaOrganizarTorres(Modelo modelo) {
    super(modelo, "Organizar Cartas Torre.");
        
    panel.setLayout(new GridLayout(1, 5));
        
    JButton button = new PTButton("Reorganizar");
    button.addActionListener( new reOrder() );
    
    Box box = Box.createHorizontalBox();
    box.setBorder(new LineBorder(Color.WHITE,5));
    box.add(Box.createVerticalGlue());
    box.add(Box.createHorizontalGlue());
    box.add(button);
    box.add(Box.createHorizontalGlue());
    box.add(Box.createVerticalGlue());
        
    add(box,BorderLayout.SOUTH);
    
  }
  
 

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaAuxiliar();
  }

  @Override
  protected int getNCartas() {
    return modelo.getPilhaAuxiliar().size();
  }

  @Override
  protected boolean clicable() {
    return true;
  }

  @Override
  protected void OnCartaClick(MouseEvent e, Carta carta){
    if (e.getButton() == MouseEvent.BUTTON1) {
      try {
        modelo.organizarTorres(carta, false);
      } catch (Exception ex) {
        JOptionPane.showMessageDialog(panel, ex.getMessage(), "Organizar Torres", JOptionPane.INFORMATION_MESSAGE);
      }
    }
    if (e.getButton() == MouseEvent.BUTTON3) {
      
      try {
        int option = JOptionPane.showConfirmDialog(panel, "A Carta Torre sera enviada para o Limbo.", "Organizar Torres", JOptionPane.OK_CANCEL_OPTION);
        if(option == JOptionPane.OK_OPTION){
          modelo.organizarTorres(carta, true);
        } 
      } catch (Exception ex) {
        JOptionPane.showMessageDialog(panel, ex.getMessage(), "Organizar Torres", JOptionPane.INFORMATION_MESSAGE);
      }
      
      
      
           
    }
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null || modelo.getState() instanceof StateOrganizarTorres) {
      super.update(o, arg);
    }
  }
  
  private class reOrder implements ActionListener {

    public reOrder() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      modelo.reOrganizarTorres();
    }    
  }
  
  
  
  
}