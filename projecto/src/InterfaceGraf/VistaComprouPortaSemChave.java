package InterfaceGraf;

import BaseCartas.Carta;
import BaseEstados.StateComprouPortaSemChave;
import Onirim.Modelo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class VistaComprouPortaSemChave extends VistaPanel implements Observer {

  JLabel lblCarta;

  public VistaComprouPortaSemChave(final Modelo modelo) {
    super(modelo, "Comprou porta, mas não tem chave!");
    modelo.addObserver(this);

    panel.setLayout(new GridLayout(2,1));
    
    //CARTA PORTA LINHA 0 COLUNA 0
    lblCarta = new JLabel();
    Box box1 = Box.createHorizontalBox();
    box1.add(Box.createVerticalGlue());
    box1.add(Box.createHorizontalGlue());
    box1.add(lblCarta);
    box1.add(Box.createHorizontalGlue());
    box1.add(Box.createVerticalGlue());
    panel.add(box1);


    JButton button = new PTButton("Continuar");
    button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        modelo.comprouPortaSemChave();
      }
    });
    
    Box box2 = Box.createHorizontalBox();
    box2.add(Box.createVerticalGlue());
    box2.add(Box.createHorizontalGlue());
    box2.add(button);
    box2.add(Box.createVerticalGlue());
    box2.add(Box.createHorizontalGlue());
    panel.add(box2);
  }

  @Override
  public void update(Observable o, Object arg) {
    if (modelo.getOnirim() == null) {
      return;
    }

    if (modelo.getState() instanceof StateComprouPortaSemChave) {
      Carta carta = modelo.getPilhaJogo().getUltima();
      lblCarta.setIcon(CartaUtils.createImageIcon(carta));
    }
  }
}
