package InterfaceGraf;

import Base.Onirim;
import Onirim.Modelo;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class VistaOnirim extends JFrame implements Observer {

  private Container cp;
  private Modelo modelo;
  VistaInicial painelInicial;
  PainelJogo painelJogo;
  CardLayout cardMain;

  //CONSTRUTORES
  public VistaOnirim(Modelo m) {
    this(m, 0, 0, 1024, 730);
  }

  public VistaOnirim(Modelo m, int x, int y, int largura, int altura) {
    super("Onirim");

    modelo = m; // liga a vista ao modelo

    modelo.addObserver(this); // regista a vista como observer do modelo

    cp = getContentPane(); // obtem o contentor desta frame
    criarMenu(modelo);

    criarObjGraf();
    
    disporVista();

    defJanela(x, y, largura, altura); // definições da janela principal   

    update(modelo, null);
  }

  private void criarObjGraf() {

    painelInicial = new VistaInicial(modelo);
    
    painelJogo = new PainelJogo(modelo);    
  }

 
  
  private void disporVista() {
    cardMain = new CardLayout();
    cp.setLayout(cardMain);
    cp.add(painelInicial, "Inicial");
    cp.add(painelJogo, "Jogo");
    
    cardMain.show(cp, "Inicial");
  }

  @Override
  public void update(Observable o, Object arg) {
    
    if (modelo.getOnirim() instanceof Onirim) {
      cardMain.show(cp, "Jogo");
    }
    if (modelo.getOnirim() == null) {
      cardMain.show(cp, "Inicial");
    }
  }

  private void defJanela(int x, int y, int largura, int altura) {
    setLocation(x, y); // define a localizacao deste componente (frame)
    setSize(largura, altura); // define as dimensoes
    setMinimumSize(new Dimension(largura, altura));
    setMaximumSize(new Dimension(largura, altura));
    setPreferredSize(new Dimension(largura, altura));
    setVisible(true); // torna visivel
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//termina a aplicacao
    validate();// dispoe de novo os seus subcomponentes    
  }

  public void criarMenu(final Modelo modelo) {

    String opcoesFileNew[] = {"Onirim Base",
                              "Onirim Base + Expansao O Livro",
                              "Onirim Base + Expansao As Torres",
                              "Onirim Base + Expansao Premonições"};

    JMenuBar menuBar = new JMenuBar();
    JMenu menuFile = new JMenu("Onirim");
    JMenu menuFileNew = new JMenu("Novo");
    JMenuItem menuFileSave = new JMenuItem("Gravar para...");
    JMenuItem menuFileLoad = new JMenuItem("Ler de...");
    
    JMenuItem subMenuFileNew[] = new JMenuItem[opcoesFileNew.length];
    JMenuItem menuFileExit = new JMenuItem("Sair");

    JMenu menuHelp = new JMenu("Ajuda");
    JMenuItem menuHelpAbout = new JMenuItem("Acerca de...");


    //Cria as sub-entradas e adiciona-as ao NEW
    for (int i = 0; i < subMenuFileNew.length; i++) {
      subMenuFileNew[i] = new JMenuItem(opcoesFileNew[i]);
      menuFileNew.add(subMenuFileNew[i]);
    }

    //Adicionar as opcoes ao menu
    setJMenuBar(menuBar);
    menuBar.add(menuFile);
    menuFile.add(menuFileNew);
    menuFile.add(menuFileSave);
    menuFile.add(menuFileLoad);
    
    menuFile.add(menuFileExit);

    menuBar.add(menuHelp);
    menuHelp.add(menuHelpAbout);

    //Criar acessos rapidos "ALT + ..."
    menuFile.setMnemonic(KeyEvent.VK_F);
    menuFileNew.setMnemonic(KeyEvent.VK_N);
    menuFileExit.setMnemonic(KeyEvent.VK_X);
    menuHelp.setMnemonic(KeyEvent.VK_H);
    menuHelpAbout.setMnemonic(KeyEvent.VK_A);


    //##########################################################################
    //LISTENNERS DOS MENUS
    menuFileSave.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        String filename;
        JFileChooser fc = new JFileChooser();
        int retorno = fc.showSaveDialog(null);
        if (retorno == JFileChooser.APPROVE_OPTION) {
          filename = fc.getSelectedFile().getAbsolutePath();
          modelo.saveToFileName(filename);
        }         
      }
    });
    
     menuFileLoad.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        String filename;
        JFileChooser fc = new JFileChooser();
        int retorno = fc.showOpenDialog(null);
        if (retorno == JFileChooser.APPROVE_OPTION) {
          filename = fc.getSelectedFile().getAbsolutePath();
          try {
            modelo.lerFromFileName(filename);
          } catch (Exception ex) {
             JOptionPane.showMessageDialog(null, ex.getMessage());
          }
        }         
      }
    });
    
    
    
    //cria 1 listener para cada entrada de File > New
    for (int i = 0; i < subMenuFileNew.length; i++) {
      subMenuFileNew[i].addActionListener(new ListenerFileNew(i));
    }

    menuFileExit.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
         cardMain.show(cp, "Inicial");
//        JOptionPane.showMessageDialog(VistaOnirim.this, "Obrigado por ter jogado o Onirim! \n\n Até breve.", "Exit", JOptionPane.PLAIN_MESSAGE);
//        System.exit(0);
      }
    });

    menuHelpAbout.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(VistaOnirim.this, "ONIRIM \n\n\n"
                + "Um original: \n"
                + "http://www.wix.com/elilon/preenbulle \n\n"
                + "Adaptado por: \n"
                + "Pedro Lopes \n"
                + "Tiago Rosado",
                  "About", JOptionPane.PLAIN_MESSAGE);
      }
    });

  }

  class ListenerFileNew implements ActionListener {

    private int opcao;

    //CONSTRUTOR
    public ListenerFileNew(int i) {
      this.opcao = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      switch (opcao) {
        case 0: modelo.novoJogoBase();  break;
        case 1: criarPainelExpansaoUm(); break;
        case 2: modelo.novoJogoExp2(); break;
        case 3: criarPainelExpansaoTres(); break;
      }
    }
  }

  private void criarPainelExpansaoUm() {
    cardMain.show(cp, "Inicial");
    JOptionPane.showMessageDialog(VistaOnirim.this, "O Livro dos Passos Perdidos e Achados - Expansão não implementada",
                                  "Expansao 1", JOptionPane.PLAIN_MESSAGE);    
  }

  private void criarPainelExpansaoTres() {
    cardMain.show(cp, "Inicial");
    JOptionPane.showMessageDialog(VistaOnirim.this, "Premonições Sombrias e Sonhos Felizes - Expansao não implementada",
                                  "Expansao 3", JOptionPane.PLAIN_MESSAGE);    
  }
}
