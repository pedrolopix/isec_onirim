package InterfaceGraf;

import BaseCartas.Cartas;
import Onirim.Modelo;



public class VistaCartasJogadas extends VistaCartas{

  
  public VistaCartasJogadas(Modelo modelo) {
    super(modelo,"Cartas Jogadas");
  }
  
  

  @Override
  protected Cartas getCartas() {
    return modelo.getPilhaCartasJogadas();
  }

  @Override
  protected int getNCartas() {
    return 8;
    //return modelo.getPilhaCartasJogadas().size();
  }

  @Override
  protected boolean clicable() {
    return false;
  }
}

