package Base;

import BaseCartas.Carta;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Baralho implements Serializable{

  private ArrayList<Carta> cartas = new ArrayList<>();

  //CONSTRUTOR
  public Baralho() {
    CriarCartas();
  }

  public ArrayList<Carta> getCartas() {
    return cartas;
  }

  public abstract void CriarCartas();

  public void Baralhar() { }
}
