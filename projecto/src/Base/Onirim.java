package Base;

import BaseCartas.Carta;
import BaseCartas.CartaLabirinto;
import BaseCartas.CartaPorta;
import BaseCartas.Cartas;
import BaseEstados.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Onirim implements Serializable{

  private State state;
  private State stateFaseJogo = new StateFaseJogo(this);
  private State stateIniciarJogo = new StateIniciarJogo(this);
  private State stateComprar = new StateComprar(this);
  private State stateComprouPorta = new StateComprouPorta(this);
  private State stateComprouPortaComChave = new StateComprouPortaComChave(this);
  private State stateComprouPortaSemChave = new StateComprouPortaSemChave(this);  
  private State stateComprouSonho = new StateComprouSonho(this);
  private State stateProfecia = new StateProfecia(this);
  private State stateProfeciaDescarta = new StateProfeciaDescarta(this);
  private State stateProfeciaOrganiza = new StateProfeciaOrganiza(this);
  private State stateGanhou = new StateGanhou(this);
  private State statePerdeu = new StatePerdeu(this);
  private State stateFinal = new StateFinal(this);
  private State stateSeguinte;
  
  private List<Baralho> baralhos = new ArrayList<>();
  private Cartas mao = new Cartas(this, "mão");
  private Cartas pilhaJogo = new Cartas(this, "jogo");
  private Cartas pilhaLimbo = new Cartas(this, "limbo");
  private Cartas pilhaDescarte = new Cartas(this, "descarte");
  private Cartas pilhaCartasJogadas = new Cartas(this, "jogadas");
  private Cartas pilhaCartasProfecia = new Cartas(this, "profecia");
  private transient ILog log;

  //CONSTRUTOR
  public Onirim() { }
  
  //GET
  public Onirim getOnirim(){
    return this;
  }
  public State getState() { return state; }
  public State getStateIniciarJogo() { return stateIniciarJogo; }
  public State getStateFaseJogo() { return stateFaseJogo; } 
  public State getStateComprar() { return stateComprar; } 
  public State getStateComprouSonho() { return stateComprouSonho; }
  public State getStateComprouPorta() { return stateComprouPorta; }
  public State getStateComprouPortaComChave() { return stateComprouPortaComChave; }
  public State getStateComprouPortaSemChave() { return stateComprouPortaSemChave; }  
  public State getStateProfecia() { return stateProfecia; }
  public State getStateProfeciaDescarta() { return stateProfeciaDescarta; }
  public State getStateProfeciaOrganiza() { return stateProfeciaOrganiza; }
  public State getStateGanhou() { return stateGanhou; }  
  public State getStatePerdeu() { return statePerdeu; }
  public State getStateFinal() { return stateFinal; }
  public State getStateSeguinte() { return stateSeguinte; }
  
  public List<Baralho> getBaralhos() { return baralhos; }
  public Cartas getMao() { return mao; }
  public Cartas getPilhaJogo() { return pilhaJogo; }
  public Cartas getPilhaLimbo() { return pilhaLimbo; }
  public Cartas getPilhaDescarte() { return pilhaDescarte; }
  public Cartas getPilhaCartasJogadas() { return pilhaCartasJogadas; }
  public Cartas getPilhaCartasProfecia() { return pilhaCartasProfecia; }  
  


  //SET
  protected void setNextState(State state) {
    if (log != null) { log.estadoMudou(this.state, state); }
    this.state = state;
    this.state.onEnter(); 
  }
  public void setLog(ILog log) { this.log = log; } 
  public void nextStateIniciarJogo() { setNextState(getStateIniciarJogo()); }
  public void nextStateFaseJogo() { setNextState(getStateFaseJogo()); }
  public void nextStateComprouSonho() { setNextState(getStateComprouSonho()); }
  public void nextStateComprouPorta() { setNextState(getStateComprouPorta()); }
  public void nextStateComprouPortaComChave() { setNextState(getStateComprouPortaComChave()); } 
  public void nextStateComprouPortaSemChave() { setNextState(stateComprouPortaSemChave); }
  public void nextStateComprar() { setNextState(stateComprar); }    
  public void nextStateProfecia() { setNextState(stateProfecia); }
  public void nextStateProfeciaDescarta() { setNextState(stateProfeciaDescarta); }
  public void nextStateProfeciaOrganiza() { setNextState(stateProfeciaOrganiza); }  
  public void nextStatePerdeu() { setNextState(statePerdeu); }
  public void nextStateGanhou() { setNextState(stateGanhou); }
  public void nextStateFinal() { setNextState(stateFinal); }

    
  //############################################################################
  //##
  //## FUNÇÕES DE COMUNICAÇÃO COM O INTERFACE TEXTO
  
  public void iniciarJogo() { //StateIniciar
    nextStateIniciarJogo();
  }
  
  public void sairDoJogo() { //StateIniciar
    this.state.sairDoJogo();
  }
  
  public void jogarCarta(Carta carta) throws Exception { //StateFaseJogo
    this.state.jogarCarta(carta);
  }

  public void descartarCarta(Carta carta) { //StateFaseJogo
    this.state.descartarCarta(carta);
  }

  public void terminarJogo() { //StateFaseJogo
    this.state.terminarJogo();
  } 
  
  public void comprouPortaSemChave() { //StateComprouPorta
    this.state.comprouPortaSemChave();
  }
  
  public void comprouPortaComChave(int opcao) { //StateComprouPorta
    this.state.comprouPortaComChave(opcao);
  }
  
  public void descartarCartaProfecia(Carta carta) { //StateProfeciaDescarta
    this.state.descartaCartaProfecia(carta);
  }

  public void organizarCartasProfecia(Carta carta) { //StateProfeciaOrganiza
    this.state.organizarCartasProfecia(carta);
  }
 
  public void comprouSonho(int opcao) throws Exception{ //StateComprouSonho
    this.state.comprouSonho(opcao);
  }
  

  public Cartas getPilhaAuxiliar() {
    return getPilhaAuxiliar();
  }
  
  public Cartas getPilhaTorres(){
    return getPilhaTorres();
  }
  
  
  
  
  
  
  //############################################################################  
  
  //SE HOUVER LIMBO JUNTA E BARALHA JOGO
  public void baralhar() {
    if (!pilhaLimbo.isEmpty()) {
      doBaralharLimbo();
      pilhaJogo.addAll(pilhaLimbo);
      pilhaLimbo.clear();
      pilhaJogo.baralhar();
      doBaralhar();
    }
  }

  //SIMBOLO != DO ULTIMO JOGADO = JOGADA VALIDA
  public boolean jogadaValida(Carta carta) {
    //Para ajudar na batota
    //se carta != Labirinto é sempre jogada valida
    if( !(carta instanceof CartaLabirinto) ){
      return true;
    }
    
    if (pilhaCartasJogadas.isEmpty()) {
      return true;
    }
    else {
      Carta ultimaJogada = pilhaCartasJogadas.get(pilhaCartasJogadas.size() - 1);
      if (carta.SameSimbolo(ultimaJogada)) {
        return false;
      }
      return true;
    }
  }

  //3 CARTAS LABIRINTO CONSECUTIVAS DA MESMA COR
  public boolean coresConsecutivas() {
    int numCores = 3;
    if (pilhaCartasJogadas.size() < numCores) {
      return false;
    }

    int ultima = pilhaCartasJogadas.size() - 1;
    int coresIguais = 1;
    Carta ultimaCarta= pilhaCartasJogadas.get(ultima);

    for (int i = ultima - 1; i > ultima - numCores; i--) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        return false;
      }
      if (pilhaCartasJogadas.get(i).SameColor(ultimaCarta)) {
        coresIguais++;
      }
    }
    if (coresIguais == 3) {
      return true;
    }
    return false;
  }

  //ESTAO 8 PORTAS NA PILHA DE CARTAS JOGADAS ?
  public boolean encontrouTodasPortas() {
    int totalPortas = 8;
    int portasJogadas = 0;

    for (int i = 0; i < pilhaCartasJogadas.size(); i++) {
      if (pilhaCartasJogadas.get(i) instanceof CartaPorta) {
        portasJogadas++;
      }
    }
    if (portasJogadas == totalPortas) {
      return true;
    }
    return false;
  }
  
  

  //INICIA MAO DE 5 CARTAS SEM EFEITO DE COMPRA
  public void iniciaMao() {
    doIniciaMao();
    do {
      Carta c = pilhaJogo.tiraUltimaCarta();
      if (c instanceof CartaLabirinto) {
        mao.add(c);
      }
      else {
        pilhaLimbo.add(c);
      }
    } while (mao.size() != 5);
  } 
  
  public void eliminaCartasDescartadas(int custo) {
    for(int i=0; i<custo; i++){
      pilhaDescarte.remove(i);
    }
  }
  
  
  //############################################################################
  //## LOG DO JOGO
  //##
  public void doJogadaInvalida(Carta carta) {
    if (log != null) {
      log.jogadaInvalida(carta);
    }
  }

  public void doEncontrouPorta(Carta carta) {
    if (log != null) {
      log.encontrouPorta(carta);
    }
  }

  public void doNaoExisteChaveNaMao() {
    if (log != null) {
      log.naoExisteChaveNaMao();
    }
  }

  public void doNaoExistePortaNoJogo() {
    if (log != null) {
      log.NaoExistePortaNoJogo();
    }
  }

  public void doBaralharLimbo() {
    if (log != null) {
      log.baralharLimbo();
    }
  }

  public void doBaralhar() {
    if (log != null) {
      log.baralhar();
    }
  }

  public void doIniciaMao() {
    if (log != null) {
      log.iniciaMao();
    }
  }

  public void doMoveCarta(Cartas origem, Carta carta, Cartas destino) {
    if (log != null) {
      log.moveCarta(origem, carta, destino);
    }
  }

  public void doGanhouJogo() {
    if (log != null) {
      log.ganhouJogo();
    }
  }

  public void doPerdeuJogo() {
    if (log != null) {
      log.perdeuJogo();
    }
  }

  public void doCartaSonhoDescartada() {
    if (log != null) {
      log.cartaSonhoDescartada();
    }
  }

  public void doCartaPortaDescartada(CartaPorta carta) {
    if (log != null) {
      log.cartaPortaDescartada(carta);
    }
  }

  public void doCartaLabirintoDescartada(CartaLabirinto carta) {
    if (log != null) {
      log.cartaLabirintoDescartada(carta);
    }
  }

  public void doNaoExistemCartasSuficientes(int nPesadelo) {
    if (log != null) {
      log.naoExistemCartasSuficientes(nPesadelo);
    }
  }

  public void doTerminou() {
    if (log != null){
      log.terminouJogo();
    }
  }

  @Override
  public String toString() {
    return "Onirim base";
  }

  

   
  
}