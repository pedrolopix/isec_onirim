package Base;

import BaseCartas.Carta;
import BaseCartas.CartaLabirinto;
import BaseCartas.CartaPorta;
import BaseCartas.Cartas;
import BaseEstados.State;

public interface ILog {

  public void estadoMudou(State oldState, State newState);

  public void jogadaInvalida(Carta carta);

  public void encontrouPorta(Carta carta);

  public void naoExisteChaveNaMao();

  public void NaoExistePortaNoJogo();

  public void baralharLimbo();

  public void baralhar();

  public void iniciaMao();

  public void moveCarta(Cartas origem, Carta carta, Cartas destino);

  public void perdeuJogo();

  public void ganhouJogo();

  public void cartaSonhoDescartada();

  public void cartaPortaDescartada(CartaPorta carta);

  public void cartaLabirintoDescartada(CartaLabirinto carta);

  public void naoExistemCartasSuficientes(int nPesadelo);

  public void terminouJogo();
}
