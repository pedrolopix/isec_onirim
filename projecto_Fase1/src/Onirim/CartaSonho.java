package Onirim;

public class CartaSonho extends Carta {

  @Override
  public void foiComprada(Onirim onirim) {
    onirim.getPilhaJogo().moverCartaPara(this, onirim.getPilhaDescarte());
    onirim.nextStateComprouSonho();
  }

  @Override
  public Simbolos getSimbolo() {
    return null;
  }

  @Override
  public Cores getCor() {
    return null;
  }

  @Override
  public String toString() {
    return "S";
  }

  @Override
  public void foiDescartada(Onirim onirim) {
    onirim.doCartaSonhoDescartada();
  }
}
