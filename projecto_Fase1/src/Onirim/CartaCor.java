package Onirim;

public abstract class CartaCor extends Carta {

  private Cores cor;

  public CartaCor(Cores cor) {
    this.cor = cor;
  }

  @Override
  public abstract void foiComprada(Onirim onirim);

  @Override
  public abstract Simbolos getSimbolo();

  @Override
  public Cores getCor() {
    return cor;
  }

  @Override
  public String toString() {
    return cor.toString().toUpperCase().substring(0, 1);
  }
}
