package Onirim;

public abstract class State {

  private Onirim onirim;
  private IExecute iexecute;

  //CONSTRUTOR
  public State(Onirim onirim) {
    this.onirim = onirim;
  }

  //GET
  public Onirim getOnirim() {
    return onirim;
  }

  //SET
  public void setIExecute(IExecute executeState) {
    this.iexecute = executeState;
  }

  public void executaEstado() {
    if (iexecute != null) {
      iexecute.executar(this);
    }
  }

  public void joga(Carta carta) {
  }

  public void descarta(Carta carta) {
  }

  public void comprou(Carta carta) {
  }
}
