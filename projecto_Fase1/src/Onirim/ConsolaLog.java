package Onirim;

public class ConsolaLog implements ILog {

  @Override
  public void naoExisteChaveNaMao() {
    System.out.println("Não existe chave na mão!");
  }

  @Override
  public void NaoExistePortaNoJogo() {
    System.out.println("Não existe porta em jogo!");
  }

  @Override
  public void baralharLimbo() {
    System.out.println("A baralhar limbo");
  }

  @Override
  public void baralhar() {
    System.out.println("A baralhar pilha de jogo");
  }

  @Override
  public void iniciaMao() {
    System.out.println("A iniciar mão");
  }

  @Override
  public void estadoMudou(State oldState, State newState) {
    System.out.println("Mudou o estado de " + oldState + " para " + newState);
  }

  @Override
  public void jogadaInvalida(Carta carta) {
    System.out.println("Não é possível jogar a carta " + carta);
  }

  @Override
  public void encontrouPorta(Carta carta) {
    System.out.println("Encontrou porta " + carta.getCor());
  }

  @Override
  public void moveCarta(Cartas origem, Carta carta, Cartas destino) {
    System.out.println("Carta " + carta + " movida de " + origem.getNome() + " para " + destino.getNome());
  }

  @Override
  public void perdeuJogo() {
    System.out.println("Assim não, acabou de perder o jogo... ooohhhhhhhhhhh!!!");
  }

  @Override
  public void ganhouJogo() {
    System.out.println("Muitos parabens! GANHOU o jogo.");
  }

  @Override
  public void cartaSonhoDescartada() {
    System.out.println("Descartado um Sonho");
  }

  @Override
  public void cartaPortaDescartada(Carta carta) {
    System.out.println("Descartada uma Porta "+ carta.getCor());
  }

  @Override
  public void cartaLabirintoDescartada(Carta carta) {
    System.out.println("Foi descartada uma carta Labirinto "+carta.getCor()+" "+carta.getSimbolo());
  }

  @Override
  public void naoExistemCartasSuficientes(int nPesadelo) {
    System.out.println("Nao ha cartas de jogo para realizar o Pesadelo "+ nPesadelo);
  }
}
