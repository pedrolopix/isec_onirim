package Onirim;

import java.util.ArrayList;

public abstract class Baralho {

  private ArrayList<Carta> cartas = new ArrayList<>();

  //CONSTRUTOR
  public Baralho() {
    CriarCartas();
  }

  public ArrayList<Carta> getCartas() {
    return cartas;
  }

  public abstract void CriarCartas();

  public void Baralhar() { }
}
